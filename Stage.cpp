#include "Stage.h"
#include "Engine/Model.h"
#include "Engine/CsvReader.h"
#include "Engine/Camera.h"

const float CAM_ARM = 25.f;

//コンストラクタ
Stage::Stage(GameObject* parent)
	:GameObject(parent, "Stage"), stageWidth_(0), stageHeight_(0)
{

	//オブジェクトが増えたとき用にループ分で
	for (int i = 0; i < 2; i++)
	{
		hModel_[i] = -1;
	}

	//Csvを使うため準備
	CsvReader csv;

	//csvファイル読み込み
	csv.Load("Assets/MapData.csv");

	//行列の大きさ結び付け
	stageWidth_ = (int)csv.GetWidth();
	stageHeight_ = (int)csv.GetHeight();

	//table_(マップ)の動的生成
	table_ = new int* [stageWidth_];

	//まず幅確保
	for (int x = 0; x < stageWidth_; x++)
	{
		table_[x] = new int[stageHeight_];
	}

	//csvから壁・床の数値を取得
	for (int x = 0; x < stageWidth_; x++)
	{
		for (int z = 0; z < stageHeight_; z++)
		{
			table_[x][z] = csv.GetValue(x, z);
		}
	}

	//原点(0,0,0,0)は左下
	transform_.position_ = XMVectorSet(((float)stageWidth_ / 2), 0.f, ((float)stageHeight_ / 2), 0.f);
	//transform_.position_ = XMVectorSet(0.f, 0.f, 0.f, 0.f);

}

//デストラクタ
Stage::~Stage()
{
	//table_の削除
	for (int x = 0; x < stageWidth_; x++)
	{
		delete[] table_[x];
	}
	delete[] table_;

}

//初期化
void Stage::Initialize()
{
	//モデルデータのロード(床)
	hModel_[FLOOR] = Model::Load("Assets/Floor.fbx", SHADER_TOON);
	assert(hModel_[FLOOR] >= 0);

	//モデルデータのロード(壁)
#ifdef _DEBUG
	hModel_[WALL] = Model::Load("Assets/BoxDefault.fbx", SHADER_TOON);
	//hModel_[WALL] = Model::Load("Assets/Wall.fbx", SHADER_DEBUG); //別の壁
#else
	//リリース用
	hModel_[WALL] = Model::Load("Assets/BoxDefault.fbx", SHADER_TOON);
#endif
	assert(hModel_[WALL] >= 0);

	//壁モデルをリストに格納
	StoreWallHandle(hModel_[WALL]);
	//StoreAllWallHandle(hModel_[WALL]);

	//俯瞰カメラ
	//マップの大きさに応じてカメラ位置調整
	Camera::SetPosition(XMVectorSet((stageWidth_ / 2.f), CAM_ARM, -(stageHeight_ / 2.f) + 11, 0.f));
	Camera::SetTarget(XMVectorSet((stageWidth_ / 2.f), 0.f, (stageHeight_ / 2.f), 0.f));
}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	//マップをcsv情報から表示
	for (int x = 0; x < stageWidth_; x++)
	{
		for (int z = 0; z < stageHeight_; z++)
		{
			int type = table_[x][z];

			//座標と配置位置を結び付け
			Transform trans = transform_;
			//大きさちょっと調整
			trans.scale_.vecY = 0.6f;
			trans.position_ = XMVectorSet((float)x, 0.f, (float)z, 0.f);

			//その座標に応じたタイプのモデルを表示
			Model::SetTransform(hModel_[type], trans);
			Model::Draw(hModel_[type]);
		}
	}

}

//開放
void Stage::Release()
{
}

//床壁判断
bool Stage::IsWall(int x, int z)
{
	return table_[x][z] == WALL;
}

//全壁モデルハンドルの格納
void Stage::StoreAllWallHandle(int hundle)
{
	for (int x = 0; x < stageWidth_; x++)
	{
		for (int z = 0; z < stageHeight_; z++)
		{
			//壁でなければ無視
			if (!IsWall(x, z))
				continue;

			//座標と配置位置を結び付け
			Transform trans = transform_;
			trans.position_ = XMVectorSet((float)x, 0.f, (float)z, 0.f);

			//トランスフォームのセット
			Model::SetTransform(hundle, trans);

			StoreWallHandle(hundle);
			//hWallModelList_;
		}
	}
}
