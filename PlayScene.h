#pragma once
#include "Engine/GameObject.h"
#include "Engine/Text.h"


class PlayScene : public GameObject
{
private:
	//テキスト
	Text* pText_;

	//制限時間
	float timeLimit;


public:
	//コンストラクタ
	PlayScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};