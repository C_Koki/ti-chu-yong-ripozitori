#include "PlayScene.h"

#include "Engine/Camera.h"
#include "Engine/CsvReader.h"
#include "Engine/Timer.h"

#include "Player.h"
#include "Enemy.h"
#include "EnemyCrawl.h"
#include "Stage.h"

const float TIMER_POS_X = 100.f;
const float TIMER_POS_Y = 10.f;
const float TIMER_SIZE = 10.f;

//コンストラクタ
PlayScene::PlayScene(GameObject * parent)
	: GameObject(parent, "PlayScene"),pText_(new Text()) ,timeLimit(60.0f)
{
}

//初期化
void PlayScene::Initialize()
{

	//ステージ
	Instantiate<Stage>(this);

	//各キャラクター
	Instantiate<Player>(this);
	Instantiate<Enemy>(this);
	Instantiate<EnemyCrawl>(this);

	//テキスト
	//pText_->Initialize();
}

//更新
void PlayScene::Update()
{
	/* 時間計測開始 */
	
	static float nowTime = 0.f;
	nowTime += Timer::GetDelta();
	static float lastUpdateTime = nowTime;

	//ゲーム内は60FPSで動いているので*60で一秒刻み
	if ((nowTime - lastUpdateTime * 60) >= 1)
	{
		lastUpdateTime = nowTime;

		timeLimit -= 1.f;
	}

	//タイマー描画
	//pText_->DrowText(TIMER_POS_X, TIMER_POS_Y, D2D1::ColorF::Red, TIMER_SIZE, timeLimit);

	if (timeLimit < 0.f)
	{
		int a = 0;
	}
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}