#pragma once
#include "Engine/GameObject.h"
#include "Engine/Math.h"


class DropItem : public GameObject
{
	//モデル番号
	int hModel_;

	//優先度
	int ITEM_PRIORITY = 0;

public:
	//コンストラクタ
	DropItem(GameObject* parent);

	//デストラクタ
	~DropItem();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//当たった時
	//引数：当たったオブジェクト
	void OnCollision(GameObject* pTarget) override;
};