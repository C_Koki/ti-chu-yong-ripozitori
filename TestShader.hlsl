// テクスチャ＆サンプラーデータのグローバル変数定義
Texture2D g_texture : register(t0); //テクスチャー
SamplerState g_sampler : register(s0); //サンプラー

// コンスタントバッファ
// DirectX 側から送信されてくる、ポリゴン頂点以外の諸情報の定義
cbuffer global
{
    float4x4 matWVP;		 //ワールド・ビュー・プロジェクションの合成行列
    float4x4 matNormal;		 //回転 × 拡大の逆行列
    float4x4 matWorld;		 //純粋なワールド行列
    float4   camPos; 		 //カメラの位置

    float4   diffuseColor;	 //ディフューズカラー（マテリアルの色）
    float4   ambientColor;	 //環境光
    float4   specularColor;	 //鏡面反射光
    float    shininess;		 //光沢度

    bool     isTexture;		 //テクスチャ貼ってあるかどうか
};

// 色と表示座標をまとめた構造体
struct VS_OUT
{
    float4 pos    : SV_POSITION;	//位置
    float4 normal : NORMAL;		    //法線
    float4 eye    : TEXCOORD1;		//視線ベクトル
    float2 uv     : TEXCOORD;		//uv情報
};

// 頂点シェーダ
VS_OUT VS(float4 pos : POSITION, float4 uv : TEXCOORD, float4 normal : NORMAL)
{
	//出力情報なので"out"Data
    VS_OUT outData;

	//wに謎の値が入っているので初期化
    normal.w = 0;

	//位置情報
    outData.pos = mul(pos, matWVP);

	//uv情報はそのまんま(DirectXではfloat4で扱う、その方が計算しやすいから)
    outData.uv = uv;

	//影がモデルについてこないように、法線の正しい計算を行う
	//法線を回転行列 * 拡大行列の逆行列で変形
    outData.normal = mul(normal, matNormal);

	//視点へのベクトル
    outData.eye = normalize(camPos - mul(pos, matWorld));

    return outData;
}

// ピクセルシェーダ
//引数：頂点シェーダの戻値
//戻値：色情報
float4 PS(VS_OUT inData) : SV_TARGET
{
	//ライト
    float4 light = float4(1, -1, 0, 0); //ライトの向き(今回はここで指定)
    light = normalize(light);       //向き情報のみ必要なので正規化

	//環境光
    float4 ambient = ambientColor; 

	//法線と面へのベクトルの角度
	//saturate…値を0〜1の間に収める機能
	//「能」「ソ」「十」などの文字コメントで終わるとエラーが起きる
    float4 LN = (dot(-light, normalize(inData.normal)) + 1 * 0.5);      //<= 明るくするため計算付与
    float4 id; //その物体自体の色

	//テクスチャの有無で分岐
    if (isTexture == true)
    {
		//あるならその色を
        id = g_texture.Sample(g_sampler, inData.uv);
    }
    else
    {
		//ないならディフューズカラーを
        id = diffuseColor;
    }

	//LNとidをひとまとめ(拡散反射光)
    float4 diffuse = LN * id; //拡散反射光
    
	//第二引数の法線に対しての反射
    float4 R = reflect(light, normalize(inData.normal)); //反射ベクトル



	//8がちょうどいいので今回はこの値
	//値を大きくすると、光沢ができない箇所が生まれる
	//shininess = 30;				    //光沢度
    float4 is = specularColor;          //ハイライトの色
    float ks = 2;                       //鏡面反射係数
    
	//R,V,光沢度,isをひとまとめ(鏡面反射光)
    float4 specular = ks * pow(saturate(dot(R, normalize(inData.eye))), shininess) * is; //鏡面反射光

	//Phongの反射モデルの計算式に当てはめる(下部の参考より)
    float4 color = ambient + diffuse + specular;


    return color;
}

/*	メモ
phongシェーディング参考:
https://ja.wikipedia.org/wiki/Phong%E3%81%AE%E5%8F%8D%E5%B0%84%E3%83%A2%E3%83%87%E3%83%AB
*/