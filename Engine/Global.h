#pragma once

//各ソースで使う解放処理
#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}

//エラー処理
#define CHECK_HRESULT(p,str) if(p) {MessageBox(nullptr, str, "エラー", MB_OK);return E_FAIL;}
