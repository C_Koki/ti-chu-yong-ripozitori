#include "Timer.h"

#pragma comment(lib, "winmm.lib")

namespace Timer
{
    static auto deltaTime = std::chrono::microseconds(0);       //デルタタイム
    static float microDiv = 1000000.f;      //マイクロ

    //現在時刻の取得
    double Timer::GetDelta()
    {
        return deltaTime.count() / microDiv;        //経過時間をマイクロ秒から秒に変換してから返す
    }

    //初期化
    void Timer::Reset()
    {
        deltaTime = std::chrono::microseconds(0);
    }

    //経過時間更新
    void Timer::UpdateFrameDelta()
    {
        static auto prevTime = std::chrono::system_clock::now();

        auto now = std::chrono::system_clock::now();

        deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(now - prevTime);

        prevTime = now;
    }

    //終了時刻計測
    double EndPlayTime(clock start, clock end)
    {
        return std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / microDiv;
    }
}

/*  メモ
*   UpdateFrameDelta()で起動からの経過時間の更新
* 　GetDeltaで現在時間取得
*   EndPlayTimeで引数間の時間の取得
*/