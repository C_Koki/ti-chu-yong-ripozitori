#pragma once
#include <string>
#include <list>
#include <vector>
#include <memory>
#include "Transform.h"
//#include "CharacterAI.h"
//#include "SphereCollider.h"

//プロトタイプ宣言(ポインタとして使うだけであればこれで十分)
class  SphereCollider;
class  CharacterAI;

class GameObject
{
	//死亡フラグ
	bool isDead_;

	//オブジェクト名
	std::string objectName_;

	//コライダーリスト(ベクターで)
	std::vector <SphereCollider*> colliders_;

	//移動速度
	float moveSpeed_;

	//優先度
	int priority_;


//継承先のみで使えるように
protected:

	//オブジェクトを入れる子リスト
	std::list <GameObject*> childList_;

	//親オブジェクト
	GameObject* pParent_;


public:
	//コンストラクタ
	GameObject();

	//コンストラクタ(名前アリ)
	//引数：�@親オブジェクト
	//		�Aオブジェクトの名前
	GameObject(GameObject* parent, const std::string& name);

	//デストラクタ
	virtual ~GameObject();

	//トランスフォーム
	Transform	transform_;

	//親オブジェクトを取得
	//戻値：親オブジェクトのアドレス
	GameObject* GetParent() { return pParent_; }



	//各継承先で使う関数
	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;
	virtual void Release() = 0;


	/*  各アクセス関数  */

	XMVECTOR GetPosition();
	XMVECTOR GetQuaternion()					{ return transform_.quaternion_; }
	XMVECTOR GetScale()							{ return transform_.scale_; }
	XMVECTOR GetWorldPosition()					{ return GetParent()->transform_.position_ + transform_.position_; }
	XMVECTOR GetWorldQuaternion()				{ return GetParent()->transform_.quaternion_ + transform_.quaternion_; }
	XMVECTOR GetWorldScale()					{ return GetParent()->transform_.scale_ + transform_.scale_; }
	XMMATRIX GetWorldMatrix()					{ return transform_.GetWorldMatrix(); }
	void SetPosition(XMVECTOR position)			{ transform_.position_ = position; }
	void SetPosition(float x, float y, float z) { SetPosition(XMVECTOR{ x, y, z }); }
	void SetQuaternion(XMVECTOR quaternion)		{ transform_.quaternion_ = quaternion; }
	void SetScale(XMVECTOR scale)				{ transform_.scale_ = scale; }
	void SetScale(float x, float y, float z)	{ SetScale(XMVectorSet(x, y, z, 0)); }
	void TransCalclation()						{ return transform_.Calclation(); }
	Transform GetTransform()					{ return transform_; }
	float GetMoveSpeed()						{ return moveSpeed_; }
	void SetMoveSpeed(float moveSpeed)			{ moveSpeed_ = moveSpeed; }
	void SetPriority(int priority)				{ priority_ = priority; }
	int GetPriority()							{ return priority_; }

	//オブジェクトの名前を渡す
	//戻値：オブジェクトの名前
	const std::string& GetObjectName(void) const;

	//上の関数は純粋仮想関数なので、そうではない「子を呼ぶ用の関数」を作る
	void UpdateSub();
	void DrawSub();
	void ReleaseSub();

	//死亡フラグ建て関数
	void KillMe();
	//子供以下を削除
	void KillAllChildren();

	//オブジェクト探索
	//引数：対象のオブジェクト名
	//戻値：対象のオブジェクト(FindChildObjectにて)
	GameObject* FindObject(std::string name);

	//オブジェクト探索
	//引数：対象のオブジェクト名
	//戻値：対象のオブジェクト
	GameObject* FindChildObject(std::string name);

	//最も上の親を取得
	//戻値：RootObject
	GameObject* GetRootjob();


	//GameObjectにコライダー追加
	//引数：インスタンス化したコライダー(中身はコライダーの中心座標と半径)
	void AddCollider(SphereCollider* collider);

	//シーン先で呼ぶ当たり判定が起こった時に呼ばれる関数
	//引数：衝突対象
	virtual void OnCollision(GameObject* pTarget) {};

	//当たり判定
	//引数：衝突対象
	void Collision(GameObject* pTarget);

	//GameObject型のオブジェクトにAIセット
	//今後違うAIやオブジェクトが出た時のためここに記述
	//引数：�@インスタンス化したAI、�A移動速度
	void AddCharacterAI(CharacterAI* ai, float moveSpeed);

	//オブジェクト先でAIが付与された時に呼ばれる関数
	//引数：搭載するオブジェクト
	virtual void BootCharacterAI(GameObject* gameObject) {};

	//AIを停止する関数
	//引数：搭載されているオブジェクト
	virtual void StopCharacterAI(GameObject* gameObject) {};

	//ゲームオブジェクト毎に必要な処理をテンプレート化
	//ゲームオブジェクト毎に必要なのでヘッダー自体に記述
	template <class T>
	GameObject* Instantiate(GameObject* parent)
	{
		T* p;
		p = new T(parent);
		p->Initialize();
		childList_.push_back(p);
		return p;	//作ったオブジェクトのアドレス返却
	}

};