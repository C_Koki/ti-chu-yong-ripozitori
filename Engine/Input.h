#pragma once

#include <dInput.h>
#include <DirectXMath.h>
#include "XInput.h"

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")
#pragma comment(lib, "Xinput.lib")

using namespace DirectX;


//キーボードやマウス、コントローラーは敵味方で別れたりはしないので一個
namespace Input
{
	//初期化
	//引数：ウィンドウハンドル
	//戻値：S_OK,E_FAIL
	HRESULT Initialize(HWND hWnd);

	//更新
	void Update();

	//引数のキーが押されているかどうか
	//引数：キーコード
	//戻値：押している間true
	bool IsKey(int keyCode);

	//引数のきーが押された瞬間
	//引数：コード
	//戻値： 押した瞬間true
	bool IsKeyDown(int keyCode);

	//キーを今放したか調べる
	//引数：調べたいキーのコード
	//戻値：放した瞬間だったらtrue
	bool IsKeyUp(int keyCode);

	//マウスボタンが押されているか調べる
	//引数：ボタン番号
	//戻値：押している間true
	bool IsMouseButton(int buttonCode);

	//マウスのボタンが押された瞬間
	//引数：ボタン番号
	//戻値：押した瞬間true
	bool IsMouseButtonDown(int buttonCode);

	//マウスのボタンが離された瞬間
	//引数：ボタン番号
	//戻値：離した瞬間true
	bool IsMouseButtonUp(int buttonCode);

	//マウスカーソルの座標を取得
	//戻値：マウスカーソルの位置
	XMVECTOR GetMousePosition();

	//マウスの座標を設定
	//引数：�@マウスのx座標
	//		�Aマウスのy座標
	void SetMousePosition(int x, int y);

	//マウスの移動量を取得
	//戻り値：マウスの移動量
	XMVECTOR GetMouseMove();

	/*　コントローラー　*/
	//コントローラーのボタンが押されているか調べる
	//引数：�@調べたいボタンの番号
	//	　：�Aコントローラーの番号
	//戻値：押されていればtrue
	bool IsPadButton(int buttonCode, int padID = 0);

	//コントローラーのボタンを今押したか調べる（押しっぱなしは無効）
	//引数：�@調べたいボタンの番号
	//	　：�Aコントローラーの番号
	//戻値：押した瞬間だったらtrue
	bool IsPadButtonDown(int buttonCode, int padID = 0);

	//コントローラーのボタンを今放したか調べる
	//引数：�@調べたいボタンの番号
	//	　：�Aコントローラーの番号
	//戻値：放した瞬間だったらtrue
	bool IsPadButtonUp(int buttonCode, int padID = 0);

	//左スティックの傾きを取得
	//引数：コントローラーの番号
	//戻値：傾き具合（-1〜1）
	XMVECTOR GetPadStickL(int padID = 0);

	//右スティックの傾きを取得
	//引数：コントローラーの番号
	//戻値:傾き具合（-1〜1）
	XMVECTOR GetPadStickR(int padID = 0);

	//左トリガーの押し込み具合を取得
	//引数：コントローラーの番号
	//戻値:押し込み具合（0〜1）
	float GetPadTrrigerL(int padID = 0);

	//右トリガーの押し込み具合を取得
	//引数：コントローラーの番号
	//戻値:押し込み具合（0〜1）
	float GetPadTrrigerR(int padID = 0);

	//解放処理
	void Release();
};
