#include <DirectXCollision.h>

#include "Fbx.h"
#include "Camera.h"
#include "Texture.h"
#include "Math.h"
#include "Global.h"

Fbx::Fbx()
	: vertexCount_(0), polygonCount_(0)
	, materialCount_(0), ppIndexData_(nullptr)
	, pMaterialList_(nullptr), pVertices_(nullptr)
	//, pVertexBuffer_(nullptr), ppIndexBuffer_(nullptr)
	//, pConstantBuffer_(nullptr)
{
}

HRESULT Fbx::Load(std::string fileName)
{
	//マネージャを生成
	FbxManager* pFbxManager = FbxManager::Create();

	//インポーターを生成
	FbxImporter *fbxImporter = FbxImporter::Create(pFbxManager, "imp");
	if (!fbxImporter->Initialize(fileName.c_str(), -1, pFbxManager->GetIOSettings()))
	{
		return E_FAIL;	//インポーター作成失敗
	}

	//シーンオブジェクトにFBXファイルの情報を流し込む
	FbxScene*	pFbxScene = FbxScene::Create(pFbxManager, "fbxscene");
	fbxImporter->Import(pFbxScene);
	fbxImporter->Destroy();

	//メッシュ情報を取得
	FbxNode* rootNode = pFbxScene->GetRootNode();	//最上位のノード取得
	FbxNode* pNode = rootNode->GetChild(0);			//一番最初の子ノード取得
	FbxMesh* mesh = pNode->GetMesh();				//ノードの中のメッシュ(形)情報取得
													//ノード内にはメッシュとマテリアル情報がある

	//各情報の個数を取得
	vertexCount_ = mesh->GetControlPointsCount();			//頂点の数
	polygonCount_ = mesh->GetPolygonCount();				//ポリゴンの数
	materialCount_ = pNode->GetMaterialCount();				//マテリアルの数
	polygonVertexCount_ = mesh->GetPolygonVertexCount();	//ポリゴン頂点インデックスの数


	//マテリアルの数ぶんリスト登録
	pMaterialList_ = new MATERIAL[materialCount_];


	//現在のカレントディレクトリを取得
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	//引数のfileNameからディレクトリ部分を取得
	char dir[_MAX_DIR];
	_splitpath_s(fileName.c_str(), nullptr, 0, dir, _MAX_DIR, nullptr, 0, nullptr, 0);
	
	//カレントディレクトリの変更(これ以降はAssetsのみを見る)
	SetCurrentDirectory(dir);

	//各工程内でエラー処理(カレントディレクトリの修正、マネージャ開放も各工程で必要)
	//頂点バッファ準備
	if(FAILED(InitVertex(mesh)))	
	{
		//ディレクトリを元に(エラーによる終了前にも必要)
		SetCurrentDirectory(defaultCurrentDir);
		//マネージャ開放(エラーによる終了前にも必要)
		pFbxManager->Destroy();

		MessageBox(nullptr, "エラー InitVertex", "エラー", MB_OK);
		return E_FAIL;
	}

	//インデックスバッファ準備
	if (FAILED(InitIndex(mesh)))	
	{
		SetCurrentDirectory(defaultCurrentDir);
		pFbxManager->Destroy();

		MessageBox(nullptr, "エラー InitIndex", "エラー", MB_OK);
		return E_FAIL;
	}

	//コンスタントバッファ準備
	if (FAILED(InitConstantBuffer()))	
	{
		SetCurrentDirectory(defaultCurrentDir);
		pFbxManager->Destroy();

		MessageBox(nullptr, "エラー InitConstantBuffer", "エラー", MB_OK);
		return E_FAIL;
	}

	//マテリアル準備
	if (FAILED(InitMaterial(pNode)))	
	{
		SetCurrentDirectory(defaultCurrentDir);
		pFbxManager->Destroy();

		MessageBox(nullptr, "エラー InitMaterial", "エラー", MB_OK);
		return E_FAIL;
	}
	

	//終わったらディレクトリを戻す
	SetCurrentDirectory(defaultCurrentDir);


	//マネージャ解放
	pFbxManager->Destroy();
	return S_OK;
}


//頂点バッファ準備
HRESULT Fbx::InitVertex(fbxsdk::FbxMesh* mesh)
{
	//頂点情報を入れる配列
	pVertices_ = new VERTEX[vertexCount_];

	//全ポリゴン
	//ポリゴンごとにループ
	for (DWORD poly = 0; poly < polygonCount_; poly++)
	{
		//3頂点分
		for (int vertex = 0; vertex < 3; vertex++)
		{
			//調べる頂点の番号
			int index = mesh->GetPolygonVertex(poly, vertex);

			//頂点の位置
			FbxVector4 pos = mesh->GetControlPointAt(index);
			//fbxにすると左右反転するため-posで修正(さらに反転)
			pVertices_[index].position = XMVectorSet((float)-pos[0], (float)pos[1], (float)pos[2], 0.0f);

			//頂点のUV
			//V差表は上下反転が必要なので0-1で上下反転
			FbxLayerElementUV* pUV = mesh->GetLayer(0)->GetUVs();
			int uvIndex = mesh->GetTextureUVIndex(poly, vertex, FbxLayerElement::eTextureDiffuse);
			FbxVector2  uv = pUV->GetDirectArray().GetAt(uvIndex);
			pVertices_[index].uv = XMVectorSet((float)uv.mData[0], (float)(1.0f - uv.mData[1]), 0.0f, 0.0f);

			//頂点の法線
			FbxVector4 Normal;
			mesh->GetPolygonVertexNormal(poly, vertex, Normal);		//ｉ番目のポリゴンの、ｊ番目の頂点の法線をゲット
			//fbxにすると法線も左右反転するため-Normalで修正(さらに反転)
			pVertices_[index].normal = XMVectorSet((float)-Normal[0], (float)Normal[1], (float)Normal[2], 0.0f);
		}
	}

	//頂点のUV
	int m_dwNumUV = mesh->GetTextureUVCount();
	FbxLayerElementUV* pUV = mesh->GetLayer(0)->GetUVs();
	if (m_dwNumUV > 0 && pUV->GetMappingMode() == FbxLayerElement::eByControlPoint)
	{
		for (int k = 0; k < m_dwNumUV; k++)
		{
			FbxVector2 uv = pUV->GetDirectArray().GetAt(k);
			pVertices_[k].uv = XMVectorSet((float)uv.mData[0], (float)(1.0f - uv.mData[1]), 0.0f, 0.0f);
		}
	}


	// 頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;
	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount_;	//型名でsize測定可
	bd_vertex.Usage = D3D11_USAGE_DYNAMIC;	
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd_vertex.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE; 
	bd_vertex.MiscFlags = 0;
	bd_vertex.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA data_vertex;
	data_vertex.pSysMem = pVertices_;

	//頂点バッファ生成に失敗すれば上に報告(強制終了)
	Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, pVertexBuffer_.GetAddressOf());

	//UsageがDEFAULT時の失敗チェック
	auto hr = Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, pVertexBuffer_.GetAddressOf());
	if (FAILED(hr)) 
	{
		auto a = 0;
	}

	return S_OK;

}

//インデックスバッファ準備
HRESULT Fbx::InitIndex(fbxsdk::FbxMesh* mesh)
{
	//マテリアル数ぶんインデックス情報の数を記憶
	ppIndexBuffer_ = new Microsoft::WRL::ComPtr<ID3D11Buffer>[materialCount_];
	ppIndexData_ = new DWORD*[materialCount_];
	
	//Mine
	indexCountEachMaterial_ = new DWORD[materialCount_];

	int count = 0;
	//インデックス情報を取得してバッファに入れる
	for (DWORD i = 0; i < materialCount_; i++)
	{
		count = 0;

		DWORD *pIndex = new DWORD[polygonCount_ * 3];
		ZeroMemory(&pIndex[i], sizeof(pIndex[i]));

		//全ポリゴン
		for (DWORD poly = 0; poly < polygonCount_; poly++)
		{
			//マテリアルの情報を調べマテリアルIDとiを比較
			FbxLayerElementMaterial* mtl = mesh->GetLayer(0)->GetMaterials();
			int mtlId = mtl->GetIndexArray().GetAt(poly);
			if (mtlId == i)
			{
				//3頂点分
				//DWORDだとunisigned intなのでマイナスに対応できない
				//=>NormalMapの時は0〜3のforで取得、x軸も反転させない
				for (int vertex = 0; vertex < 3; vertex++)		//<= 通常のもの(2〜0)とは逆にしている
				{
					pIndex[count + vertex] = mesh->GetPolygonVertex(poly, vertex);
				}
				count += 3;		//<=時計回りでも反転でも実行確認済
			}
		}

		//インデックス情報数代入
		//マテリアルごとにカウント数(頂点数)が入る

		//Mine
		indexCountEachMaterial_[i] = count;

		// インデックスバッファを生成する
		D3D11_BUFFER_DESC   bd;
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(int) * count;
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA InitData;
		InitData.pSysMem = pIndex;
		InitData.SysMemPitch = 0;
		InitData.SysMemSlicePitch = 0;
		Direct3D::pDevice->CreateBuffer(&bd, &InitData, ppIndexBuffer_[i].GetAddressOf());

		ppIndexData_[i] = new DWORD[count];		//ここのcountがマテリアル毎のインデックス数
		memcpy(ppIndexData_[i], pIndex, sizeof(DWORD) * count);
		delete[] pIndex;
	}

	return S_OK;
}

//コンスタントバッファ準備
HRESULT Fbx::InitConstantBuffer()
{
	//Quadクラスと同じ
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	// コンスタントバッファの作成
	Direct3D::pDevice->CreateBuffer(&cb, nullptr, pConstantBuffer_.GetAddressOf());

	return S_OK;
}




//マテリアル準備
//MaterialListをいろいろ扱っているので先に
HRESULT Fbx::InitMaterial(fbxsdk::FbxNode* pNode)
{

	for (DWORD i = 0; i < materialCount_; i++)
	{
		//初期化
		ZeroMemory(&pMaterialList_[i], sizeof(pMaterialList_[i]));

		//i番目のマテリアル情報を取得
		FbxSurfaceMaterial* pMaterial = pNode->GetMaterial(i);

		//i番目のマテリアル情報を取得(Phong版)
		FbxSurfacePhong* pPhong = (FbxSurfacePhong*)pMaterial;

		//diffuseのほかの要素も同じように追加
		//phong側にディフューズと環境光準備
		//各光の反射の値を取得
		FbxDouble3  diffuse = pPhong->Diffuse;
		FbxDouble3	ambient = pPhong->Ambient;

		//取得した反射値をマテリアルリストにコピー
		pMaterialList_[i].diffuse = XMFLOAT4((float)diffuse[0], (float)diffuse[1], (float)diffuse[2], 1.0f);
		pMaterialList_[i].ambient = XMFLOAT4((float)ambient[0], (float)ambient[1], (float)ambient[2], 1.0f);
		pMaterialList_[i].specular = XMFLOAT4(0, 0, 0, 0);
		pMaterialList_[i].shininess = 0;

		//phongシェーディングだったら
		if (pMaterial->GetClassId().Is(FbxSurfacePhong::ClassId))
		{
			//スペキュラーの色と光沢度を取る
			FbxDouble3 specular = pPhong->Specular;
			pMaterialList_[i].specular = XMFLOAT4((float)specular[0], (float)specular[1], (float)specular[2], 1.0f);
			pMaterialList_[i].shininess = (float)pPhong->Shininess;
		}

		//テクスチャの準備
		InitTexture(pMaterial, i);

	}
	return S_OK;
}

//テクスチャ準備
HRESULT Fbx::InitTexture(fbxsdk::FbxSurfaceMaterial* pMaterial, const DWORD &i)
{
	pMaterialList_[i].pTexture = nullptr;

	//ディフューズテクスチャ
	{
		//テクスチャ情報
		FbxProperty  lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sDiffuse);	//カラーに指定した情報を入れる

		//テクスチャの数数
		//緻密な解像度にしたい場合は複数貼る場合も
		int fileTextureCount = lProperty.GetSrcObjectCount<FbxFileTexture>();

		//テクスチャあり
		//fileTextureCountが0かどうかで判断
		if (fileTextureCount > 0)
		{
			FbxFileTexture* textureInfo = lProperty.GetSrcObject<FbxFileTexture>(0);
			//ファイル名が入る
			//=>ファイル名を取得するために…カレントディレクトリの設定
			const char* textureFilePath = textureInfo->GetRelativeFileName();

			//ファイル名+拡張だけにする
			char name[_MAX_FNAME];	//ファイル名
			char ext[_MAX_EXT];		//拡張子
			_splitpath_s(textureFilePath, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
			wsprintf(name, "%s%s", name, ext);


			//ファイルからテクスチャ作成
			pMaterialList_[i].pTexture = new Texture;
			pMaterialList_[i].pTexture->Load(name);
		}

		//テクスチャ無し
		else
		{
			//マテリアルリストの中身をnullptrに
			pMaterialList_[i].pTexture = nullptr;

			//マテリアルの色
			//FbxSurfaceLambert* pMaterial = (FbxSurfaceLambert*)pNode->GetMaterial(i);
			//FbxDouble3  diffuse = pMaterial->Diffuse;
			//pMaterialList_[i].diffuse = XMFLOAT4((float)diffuse[0], (float)diffuse[1], (float)diffuse[2], 1.0f);
		}
	}

	return S_OK;
}

//描画
void Fbx::Draw(Transform & transform, SHADER_TYPE shaderType)
{
	//使用するシェーダーのセットを指定する
	Direct3D::SetShaderHundle(shaderType);

	//Draw毎にtransform計算
	transform.Calclation();

	//頂点バッファ
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	//第三引数はアドレスなので、"GetAddressOf"を使う
	Direct3D::pContext->IASetVertexBuffers(0, 1, pVertexBuffer_.GetAddressOf(), &stride, &offset);


	//使用するコンスタントバッファをシェーダに伝える
	Direct3D::pContext->VSSetConstantBuffers(0, 1, pConstantBuffer_.GetAddressOf());
	Direct3D::pContext->PSSetConstantBuffers(0, 1, pConstantBuffer_.GetAddressOf());

	//コンスタントバッファに各データ受け渡し
	for (DWORD i = 0; i < materialCount_; i++)
	{
		// インデックスバッファーをセット
		stride = sizeof(int);
		offset = 0;
		Direct3D::pContext->IASetIndexBuffer(ppIndexBuffer_[i].Get(), DXGI_FORMAT_R32_UINT, 0);

		//コンスタントバッファに渡す情報
		D3D11_MAPPED_SUBRESOURCE pdata;		//サブリソースへのデータ
		CONSTANT_BUFFER cb;					//カメラと結び付け


		//ワールドビュー座標の合成行列
		cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix() * Camera::GetViewMatrix() * Camera::GetProjectionMatrix());
		
		//法線情報は後程必要
		cb.matNormal = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));		//オブジェクトの変形に合わせて法線を変形させるための行列(影も対応させる)
		
		//回転行列 * 拡大行列の逆行列
		cb.matWorld = XMMatrixTranspose(transform.GetWorldMatrix());

		//XMVectorからXMFLOAT4に変換するときはStore、逆の時はLoad(値は切り捨て)
		XMStoreFloat4(&cb.comPos, Camera::position_);

		//追加したphongシェーディング用の要素もhlslから持ってくる
		cb.diffuseColor = pMaterialList_[i].diffuse;
		cb.ambientColor = pMaterialList_[i].ambient;
		cb.specularColor = pMaterialList_[i].specular;
		cb.shininess = pMaterialList_[i].shininess;
		cb.isTexture = pMaterialList_[i].pTexture != nullptr;


		//データ更新の停止
		Direct3D::pContext->Map(pConstantBuffer_.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);	// GPUからのデータアクセスを止める
		memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));						// データを値を送る



		//マテリアルカウントごとにテクスチャの有無を確認してフラグ建て
		if (cb.isTexture == TRUE) 
		{
			//trueならいつも通り
			//サンプラー（テクスチャの表示の仕方）を設定
			ID3D11SamplerState* pSampler = pMaterialList_[i].pTexture->GetSampler();
			Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);

			//シェーダーリソースビュー（テクスチャ）を設定
			ID3D11ShaderResourceView* pSRV = pMaterialList_[i].pTexture->GetSRV();
			Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);

		}
		else 
		{
			//falseならマテリアルの色を入れる
			cb.diffuseColor = pMaterialList_[i].diffuse;
		}



		//データ更新再開
		Direct3D::pContext->Unmap(pConstantBuffer_.Get(), 0);									



		//貼り付ける画像の指定
		ID3D11ShaderResourceView* pSRV = Direct3D::pToonTex->GetSRV();
		//引数：hlsl側で設定したID、配列で渡すときの要素数(多分)
		Direct3D::pContext->PSSetShaderResources(1, 1, &pSRV);


		//インデックスバッファセット(どれを使うか)
		Direct3D::pContext->IASetIndexBuffer(ppIndexBuffer_[i].Get(), DXGI_FORMAT_R32_UINT, 0);



		//ポリゴンメッシュ描画
		//第一引数をマテリアル数を入れる配列に
		Direct3D::pContext->DrawIndexed(indexCountEachMaterial_[i], 0, 0);
	}

}

void Fbx::Release()
{
	//SAFE_DELETEは配列に対応してないため個別で
	delete[] pMaterialList_;
}

void Fbx::RayCast(RayCastData * rayData)
{
	/* 1フレーム前後のレイデータと比較したい場合はここに初期化処理を書いてはいけない */

	//当たっていないていで初期化
	rayData->hit = false;

	//レイの向きを正規化
	rayData->dir = XMVector3Normalize(rayData->dir);

	//マテリアル毎
	for (DWORD i = 0; i < materialCount_; i++)
	{
		//そのマテリアルのポリゴン毎
		//=>ここでエラー(ppIndexData)
		for (DWORD j = 0; j < indexCountEachMaterial_[i] / 3; j++)
		{
			//3頂点
			//3の倍数毎に頂点情報は並ぶので任意の三角形の頂点情報を見たいときは*3
			XMVECTOR v0 = pVertices_[ppIndexData_[i][j * 3 + 0]].position;
			XMVECTOR v1 = pVertices_[ppIndexData_[i][j * 3 + 1]].position;
			XMVECTOR v2 = pVertices_[ppIndexData_[i][j * 3 + 2]].position;

			//ここにdistの格納先を準備することで値が入っていない時は0を返している
			//=>結果としてゼロ距離扱いになって常にisWallrange内と判断(今は大丈夫)
			float dist = 0.f;
			bool hit = false;

			//その三角形との判定(最後の引数で距離受取)
			//rayData->hit = Math::Intersect(rayData->start, rayData->dir, v0, v1, v2, &dist);
			
			//DirectX内のレイキャスト（比較的重い）
			hit = DirectX::TriangleTests::Intersects(rayData->start, rayData->dir, v0, v1, v2, dist);

			//当たってかつdistがマイナスでなければ終わり
			if (hit && dist < rayData->dist)
			{
				rayData->hit = true;

				rayData->dist = dist;

				return;
			}
		}
	}
}
