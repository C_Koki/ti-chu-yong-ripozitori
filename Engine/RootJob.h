#pragma once
#include "GameObject.h"
class RootJob :	public GameObject
{
//publicでなければアクセス不可
public:
	//コンストラクタ
	RootJob();
	//デストラクタ
	~RootJob();

	// GameObject を介して継承
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

