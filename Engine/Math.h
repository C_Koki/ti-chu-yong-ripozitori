#pragma once
#include "Transform.h"	//DirectXMath.hの代わり

//ポテンシャル関数のパラメータ
struct PotentialData
{
	 float attract_;		//引力
	 float repuls_;			//斥力
	 float decayAttract_;	//引力の減衰度合い
	 float decayRepuls_;	//斥力の減衰度合い
	 float dist_;			//距離
};


namespace Math
{
	//行列式(３つのベクトルを行列に変換)
	//引数：�@ベクトルa、�Aベクトルb、�Bベクトルc
	//戻値：計算された行列
	float Det(XMVECTOR a, XMVECTOR b, XMVECTOR c);

	//三角形と線分の交差判定
	//引数：�@レイの発射点、�Aレイのベクトル、�Bポリゴン三角形の頂点(1)、
	//		�Cポリゴン三角形の頂点(2)、�Dポリゴン三角形の頂点(3)、originから交点までの距離(<-アドレスで受渡)
	//戻値：当たったか否か
	bool Intersect(CXMVECTOR origin, CXMVECTOR ray, CXMVECTOR v0, CXMVECTOR v1, CXMVECTOR v2, float *distance);

	//線形補間
	//Lerp(x,y,s) … sの値を0〜1にすると結果がx〜yに近づく
	//引数：�@線形補間開始の値、�A線形補間終了の値、�B補間係数
	//戻値：補間後の値
	float Lerp(float x, float y, float s);

	//ポテンシャル関数
	//引数：ポテンシャル関数のデータ
	//戻値：引力または斥力(正負で算出)
	float GetPotensial(PotentialData param);

};

