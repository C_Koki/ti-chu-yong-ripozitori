#pragma once
#include <DirectXMath.h>

//xyz軸を扱いやすいように準備
#define vecX m128_f32[0] 
#define vecY m128_f32[1] 
#define vecZ m128_f32[2] 


using namespace DirectX;

//位置、向き、拡大率などを管理するクラス
class Transform
{
public:
	XMMATRIX	matTranslate_;	//移動行列
	XMMATRIX	matRotate_;		//回転行列	
	XMMATRIX	matScale_;		//拡大行列
	XMVECTOR	position_;		//位置
	XMVECTOR	rotate_;		//向き
	XMVECTOR	quaternion_;	//クォータニオン(回転・姿勢制御)
	XMVECTOR	scale_;			//拡大率
	Transform*	pParent_;		//親オブジェクトの情報


	
	//コンストラクタ
	Transform();

	//デストラクタ
	~Transform();

	//各行列の計算
	void Calclation();

	//ワールド行列を取得
	//戻値：その時点でのワールド行列
	XMMATRIX GetWorldMatrix();

	//オイラー角からクォータニオン
	//引数：変換元のベクトル
	//戻値：姿勢を表すクォータニオン
	void SetRotate(CXMVECTOR vector);

	//回転軸と回転量からクォータニオン生成
	//引数：�@元のベクトル
	//		�A回転度合い
	void SetRotateAxis(CXMVECTOR vector, float angle);

	//少しずつ加算するクォータニオン生成
	//引数：元のベクトル
	void SetAdjustRotate(CXMVECTOR vector);

	//球面線形補間(行きたいクォータニオンへratioずつ回転)
	//引数：�@行先のクォータニオン
	//		�A回転度合い
	void SetSlerpRotate(CXMVECTOR quaternion, float ratio);

	//クォータニオンからオイラー角に変換
	XMVECTOR QuaternionToEuler(CXMVECTOR quaternion);

};