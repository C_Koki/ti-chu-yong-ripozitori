#pragma once
#include <iostream>
#include "Direct3D.h"

//データ型をいれる列挙
enum class DATA_TYPE
{
	INT,
	FLOAT,
	STRING,
};

class Text
{

private :
	
	ID2D1DeviceContext*		pDeviceContext_;	//デバイスコンテキスト
	ID2D1SolidColorBrush*	pSolidBrush_;		//ブラシ
	IDWriteTextFormat*		pTextFormat_;		//テキストフォーマット
	
	float fontSize;

public :
	//初期化
	//テキストフォーマットの指定、描画に必要な変数の取得
	void Initialize();

	//テキスト描画
	//タイマー描画の場合、内容の型をfloat(数値型)にしないといけないため
	//型を変換可能なテンプレの形にすべき
	//引数：�@X座標、�AY座標、�B色、�Cサイズ、�D内容
	template <typename T>
	void DrowText(float x, float y, D2D1::ColorF::Enum color, float size, const T text);

	
};



/*  メモ
*   Direct3DのEndDrawで試したやり方を型・座標・フォントサイズ・内容を引数で指定できるように
	テンプレートを用いた型に合わせた描画を行うには型別に処理を分ける必要
	数値型なら第三引数をfに、文字列ならsにする必要がある
	作るとしたら整数型、小数点型、文字列型の三つ
	型の取得はtypeid(型).name()で可能らしい。この場合型名の文字列で返却される
*/

