#pragma once

#include <DirectXMath.h>
#include "Texture.h"
#include "Transform.h"
using namespace DirectX;


class Quad
{
protected:	//privateと異なり継承先でも書き換えることができる


	//コンスタントバッファー
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;
		XMMATRIX	matW;
	};

	//頂点情報
	struct VERTEX
	{
		XMVECTOR position;	//位置とUVで1セット
		XMVECTOR uv;
		XMVECTOR normal;
	};



	//変数(ポインタ)は後でコンストラクタで初期化
	ID3D11Buffer* pVertexBuffer_;	//頂点バッファ
	ID3D11Buffer* pIndexBuffer_;	//インデックスバッファ
	ID3D11Buffer* pConstantBuffer_;	//コンスタントバッファ
	Texture* pTexture_;				//テクスチャのポインタ
	int indexNum;					//インデックス情報の数を入れる変数

public:
	Quad();
	virtual ~Quad();
	virtual HRESULT Initialize();
	void Draw(Transform& transform);
	void Release();
};