#include "Model.h"

//モデル
namespace Model
{
	//モデルデータを動的配列で確保
	std::vector<ModelData*>	datas;

	//モデルのロード
	int Load(std::string fileName, SHADER_TYPE shaderType)
	{
		ModelData* pData = new ModelData;

		//ファイル名登録
		pData->fileName = fileName;

		//シェーダタイプ登録
		pData->shaderType = shaderType;

		for (int i = 0;i < datas.size(); i++)
		{
			if (datas[i]->fileName == fileName)
			{
				pData->pFbx = datas[i]->pFbx;
				break;			
			}
		}

		//FBXのポインタがない場合、FBX.cppの方からファイル名でモデルロード
		if (pData->pFbx == nullptr)
		{
			pData->pFbx = new Fbx;
			pData->pFbx->Load(fileName);
		}

		//モデルデータ配列に格納
		datas.push_back(pData);

		//モデルデータ配列を渡す
		return (int)datas.size() - 1;
	}

	//モデルでTransformが扱えるように結び付け
	void SetTransform(int handle, Transform & transform)
	{
		//モデルハンドルが範囲外の場合はしない
		if (handle < 0 || handle > datas.size()) return;

		datas[handle]->transform = transform;

		datas[handle]->transform.Calclation();
	}

	//描画
	void Draw(int handle)
	{
		//行列計算
		datas[handle]->transform.Calclation();

		//FBX側からTransformと描画シェーダーを結びつけて描画
		datas[handle]->pFbx->Draw(datas[handle]->transform, datas[handle]->shaderType);
	}

	//描画(デバッグ)
	void DebugDraw(int handle)
	{
		//行列計算
		datas[handle]->transform.Calclation();

		//FBX側からTransformと描画シェーダーを結びつけて描画
		datas[handle]->pFbx->Draw(datas[handle]->transform, datas[handle]->shaderType);

	}

	//解放処理
	void Release(int handle)
	{
		//datasがnullptrならそのまま
		if (datas[handle] == nullptr)
		{
			return;
		}

		//SAFE_DELETEのかわり
		delete (datas[handle]);

		datas[handle] = nullptr;
	}

	//すべて解放
	void AllRelease()
	{
		//モデルデータのサイズぶんループ
		for (int i = 0; i < datas.size(); i++)
		{
			//datasがnullptrでなければ解放処理
			if (datas[i] != nullptr)
			{
				Release(i);
			}
		}

		datas.clear();
	}

	//Model側で対象のモデル番号(ハンドル)で指定して使えるように
	void RayCast(int hundle, RayCastData *rayData)
	{	
		//このままではローカル座標上のものなのでワールド座標に対応させる
		//=>ワールド行列の逆行列を用いて計算
		//�@レイの通過点を求める
		XMVECTOR passP = rayData->start + rayData->dir;

		//�Aワールド行列の逆行列を求める	※モデルを引数で指定しているためその対象モデルのみへのレイキャスト
		//モデルをハンドルから指定して、それに当たった時だけをみている
		XMMATRIX wInv = XMMatrixInverse(nullptr, datas[hundle]->transform.GetWorldMatrix());

		//�BrayDataのstartをワ逆で変換する
		rayData->start = XMVector3TransformCoord(rayData->start, wInv);

		//�C通過点をワ逆で変換する
		passP = XMVector3TransformCoord(passP, wInv);

		//�DrayDataのdirに�Bから�Cに向かうベクトルを入れる
		rayData->dir = passP - rayData->start;
		
		//Fbxのレイキャスト（レイキャストによって算出されたデータが格納される）
		datas[hundle]->pFbx->RayCast(rayData);

		
	}
}