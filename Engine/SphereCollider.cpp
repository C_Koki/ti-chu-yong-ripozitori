#include "SphereCollider.h"

SphereCollider::SphereCollider(XMVECTOR position,float radius)
	:pGameObject_(nullptr)
{
	//位置と大きさ結び付け
	position_ = position;
	size_ = XMVectorSet(radius, radius, radius, 0);
}

SphereCollider::~SphereCollider()
{
}

//対象のオブジェクトをセット
void SphereCollider::SetTargetObject(GameObject * gameObject)
{
	pGameObject_ = gameObject;
}

//オブジェクト衝突判定
bool SphereCollider::HitTargetObject(SphereCollider * object, SphereCollider * target)
{
	//オブジェクト間の距離dを、ベクトルの長さで取得
	XMVECTOR d = (object->position_ + object->pGameObject_->GetPosition()) 
		- (target->position_ + target->pGameObject_->GetPosition());
	d = XMVector3Length(d);

	//距離dが各オブジェクトの半径(サイズ)の和より小さければ、衝突している
	if (d.vecX < (object->size_.vecX + target->size_.vecX))
	{
		return true;
	}
	return false;

}

//当たった時に呼ばれる
bool SphereCollider::IsHit(SphereCollider * target)
{
	return HitTargetObject((SphereCollider*)target, this);
}
