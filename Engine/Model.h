#pragma once
#include <string>
#include <vector>
#include "Transform.h"
#include "Fbx.h"
//#include "Direct3D.h"

namespace Model
{
	//モデルデータを入れる構造体
	struct ModelData
	{
		Transform transform;	//描画内容を操作するTransfrom
		std::string fileName;	//ファイル名を入れる文字列
		Fbx* pFbx;				//Fbxのポインタ
		SHADER_TYPE shaderType;	//シェーダを指定できるように

		//構造体内にもコンストラクタ生成可能
		ModelData(): pFbx(nullptr)
		{
		}

	};

	//Fbxのロード
	//引数：ファイル名、読み込むシェーダのタイプ
	//戻値：Fbxのモデル番号
	int Load(std::string fileName, SHADER_TYPE shaderType);

	//モデルのTransfromセット
	//引数：モデルのハンドル、当たる対象のTransfrom参照
	void SetTransform(int handle,Transform& transform);

	//モデル描画
	//引数：モデルのハンドル、Transfrom参照
	void Draw(int handle);

	//デバッグ用の描画(ワイヤーフレーム)
	//引数：モデルのハンドル
	void DebugDraw(int handle);

	//モデル解放
	//引数：モデルのハンドル、Transfrom参照
	void Release(int handle);

	//全消去
	void AllRelease();

	//レイキャスト
	//引数：�@対象のモデルハンドル
	//		�Aレイキャスト用の構造体データ
	void RayCast(int hundle, RayCastData *rayData);

	//モデルハンドルを確認するレイ
	//引数：�@レイキャスト用の構造体データ
	//		�A当てる先のモデルのハンドル�@
	//		�B当てる先のモデルのハンドル�A
	void RayComparison(RayCastData *rayData, int hundle1, int hundle2);

	//レイキャストデータ(dist)の比較
	//bool DistCmp(const RayCastData& a, const RayCastData& b) { return a.dist < b.dist; }
};