#pragma once

#include <d3d11.h>
#include <fbxsdk.h>
#include <string>
#include <wrl/client.h>
#include "Transform.h"
#include "Direct3D.h"
//#include "Texture.h"

#pragma comment(lib, "LibFbxSDK-MT.lib")
#pragma comment(lib, "LibXml2-MT.lib")
#pragma comment(lib, "zlib-MT.lib")

class Texture;

//レイキャスト用構造体
struct RayCastData
{
	XMVECTOR	start;	//レイ発射位置
	XMVECTOR	dir;	//レイの向きベクトル
	float		dist;	//衝突点までの距離
	BOOL        hit;	//レイが当たったか

	RayCastData() { dist = 999.0f; }
};

class Fbx
{

	//マテリアル
	struct MATERIAL
	{
		//DWORD		polygonCount;	//ポリゴン数  =>  配列外でも扱いたいので別途準備
		Texture*	pTexture;	//テクスチャ
		XMFLOAT4	diffuse;	//色を入れる変数	
		XMFLOAT4	ambient;	//環境光の色	
		XMFLOAT4	specular;	//透明度	
		float		shininess;	//白熱光
	};

	//シェーダーに渡す情報
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;
		XMMATRIX	matNormal;
		XMMATRIX	matWorld;
		XMFLOAT4	comPos;
		XMFLOAT4	diffuseColor;
		XMFLOAT4	ambientColor;
		XMFLOAT4	specularColor;
		float		shininess;
		BOOL		isTexture;
	};

	//頂点情報
	struct VERTEX
	{
		XMVECTOR position;
		XMVECTOR uv;
		XMVECTOR normal;	//3Dなので法線情報も
	};

	DWORD vertexCount_;				//頂点数
	DWORD polygonCount_;			//ポリゴン数
	DWORD materialCount_;			//マテリアルの個数
	DWORD *indexCountEachMaterial_;	//マテリアル毎のインデックス数

	DWORD polygonVertexCount_;	//ポリゴン頂点インデックス数

	//ID3D11系の解放の為スマートポインタ使用
	Microsoft::WRL::ComPtr<ID3D11Buffer> pVertexBuffer_;	//頂点情報
	Microsoft::WRL::ComPtr<ID3D11Buffer> *ppIndexBuffer_;	//頂点順番 バッファ型のポインタの配列
	Microsoft::WRL::ComPtr<ID3D11Buffer> pConstantBuffer_;	//コンスタントバッファ
	MATERIAL* pMaterialList_;								//マテリアル情報を入れるリスト

	VERTEX* pVertices_;		//頂点情報	
	DWORD** ppIndexData_;	//インデックス情報

	HRESULT	InitVertex(fbxsdk::FbxMesh * mesh);									//頂点バッファ
	HRESULT	InitMaterial(fbxsdk::FbxNode * pNode);								//マテリアル
	HRESULT InitTexture(fbxsdk::FbxSurfaceMaterial* pMaterial, const DWORD &i);	//テクスチャ
	HRESULT	InitIndex(fbxsdk::FbxMesh * mesh);									//インデックスバッファ
	HRESULT	InitConstantBuffer();												//コンスタントバッファ

public:
	Fbx();
	HRESULT Load(std::string fileName);							//ファイル名をmainで書けるよう引数をstring
	void    Draw(Transform& transform, SHADER_TYPE shaderType);	//描画関数
	void    Release();											//解放処理

	//全ポリゴンとレイの衝突判定
	void RayCast(RayCastData* rayData);

};

