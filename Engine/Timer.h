#pragma once
#include <chrono>
#define clock std::chrono::time_point<std::chrono::system_clock>

namespace Timer
{
	//フレームのデルタタイム
	double GetDelta();

	//シーンタイマーのリセット
	void Reset();

	//デルタタイムの更新
	void UpdateFrameDelta();

	//プレイ時間決定
	//引数：�@開始時刻、�A終了時刻
	double EndPlayTime(clock start, clock end);
};

/*  メモ
*   正確な時刻を計測するためにchrono使用
* 　チーム制作のもの参考
*/