#include "Transform.h"

Transform::Transform()
{
	matTranslate_ = XMMatrixIdentity();
	matRotate_ = XMMatrixIdentity();
	matScale_ = XMMatrixIdentity();
	position_ = XMVectorSet(0, 0, 0, 0);
	rotate_ = XMVectorSet(0, 0, 0, 0);
	quaternion_ = XMQuaternionIdentity();
	scale_ = XMVectorSet(1, 1, 1, 0);
}

Transform::~Transform()
{
}

//Transform情報を一律計算
void Transform::Calclation()
{
	//移動行列
	matTranslate_ = XMMatrixTranslation(
		position_.vecX,
		position_.vecY,
		position_.vecZ
	);
	
	//回転行列
	XMMATRIX rotateX, rotateY, rotateZ;
	rotateX = XMMatrixRotationX(XMConvertToRadians(rotate_.vecX));
	rotateY = XMMatrixRotationY(XMConvertToRadians(rotate_.vecY));
	rotateZ = XMMatrixRotationZ(XMConvertToRadians(rotate_.vecZ));

	//クォータニオンから行列を生成
	matRotate_ = XMMatrixRotationQuaternion(quaternion_);

	//拡大縮小
	matScale_ = XMMatrixScaling(scale_.vecX, scale_.vecY, scale_.vecZ);
}

//ワールド行列の取得
XMMATRIX Transform::GetWorldMatrix()
{
	//計算合わせ
	Calclation();
	if (pParent_)
	{
		return  matScale_ * matRotate_ * matTranslate_ * pParent_->GetWorldMatrix();
	}

	//ここも順番によって結果が異なる
	//=>拡大縮小優先
	return matScale_ * matRotate_ * matTranslate_;
}

//ベクトル(オイラー角)からクォータニオンへの変換
void Transform::SetRotate(CXMVECTOR vector)
{
	//オイラー角をクォータニオンに変換
	quaternion_ = XMQuaternionRotationRollPitchYawFromVector(vector);
}

//軸回転
void Transform::SetRotateAxis(CXMVECTOR vector, float angle)
{
	//軸(vector)を中心にangleぶん回転(時計回りに計算)
	quaternion_ = XMQuaternionRotationAxis(vector, angle);
}

//加算していく姿勢
void Transform::SetAdjustRotate(CXMVECTOR vector)
{
	//動いてほしい分
	XMVECTOR distination = XMQuaternionRotationRollPitchYawFromVector(vector);

	//+=(加算)と同じイメージ
	quaternion_ = XMQuaternionMultiply(quaternion_, distination);

}

//クォータニオン間の補間
void Transform::SetSlerpRotate(CXMVECTOR quaternion, float ratio)
{
	//�@引数から�A引数へratioぶん回転
	quaternion_ = XMQuaternionSlerp(quaternion_, quaternion, ratio);

}

//クォータニオンからオイラーへ変換
XMVECTOR Transform::QuaternionToEuler(CXMVECTOR quaternion)
{
	float q0q0 = quaternion_.vecX * quaternion_.vecX;
	float q0q1 = quaternion_.vecX * quaternion_.vecY;
	float q0q2 = quaternion_.vecX * quaternion_.vecZ;
	float q0q3 = quaternion_.vecX * quaternion_.m128_f32[3];
	float q1q1 = quaternion_.vecY * quaternion_.vecY;
	float q1q2 = quaternion_.vecY * quaternion_.vecZ;
	float q1q3 = quaternion_.vecY * quaternion_.m128_f32[3];
	float q2q2 = quaternion_.vecZ * quaternion_.vecZ;
	float q2q3 = quaternion_.vecZ * quaternion_.m128_f32[3];
	float q3q3 = quaternion_.m128_f32[3] * quaternion_.m128_f32[3];

	rotate_.vecX = atan2(2.0 * (q2q3 + q0q1), q0q0 - q1q1 - q2q2 + q3q3);
	rotate_.vecY = asin(2.0 * (q0q2 - q1q3));
	rotate_.vecZ = atan2(2.0 * (q1q2 + q0q3), q0q0 + q1q1 - q2q2 - q3q3);

	return XMVectorSet(rotate_.vecX, rotate_.vecY, rotate_.vecZ, 0);
}


/*	メモ
	クォータニオンを扱うには使いたい回転姿勢を持ったクォータニオンを生成する。

	向きに応じた回転を可能にするためにベクトルの代わりにクォータニオンを使った関数を準備
	=>オイラー角計算とは別でそれをクォータニオンに変換するものも準備したほうが便利
	=>クォータニオンの回転計算はクォータニオン同士の掛け算で行う

	クォータニオンの掛け算(q * pなど)は、qの回転姿勢をさらにqで回転するかたち。
	クォータニオン×ベクトルは、そのベクトルがクォータニオンの回転を行うかたち
	=>オイラー角感覚で扱いたいのであれば、動的回転姿勢×入力回転姿勢のかたちが適切

	クォータニオンの内積は角度を表すベクトルが算出される

	クォータニオンは部分的に操作できないのでX,Y,Zを操作したければ各それ用の関数を準備
	今のところangleは全てラジアンなので変換もしくはラジアン用の関数オイラー角用の関数準備

	GameObjectの扱うTransformはこの場合、親からのローカル座標
	=>ワールド座標にしたいならGetWorldMatrixを
*/
