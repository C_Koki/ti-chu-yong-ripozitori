#include <iostream>
#include <cstdio>

#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>

#include "CharacterAI.h"
#include "CsvReader.h"
#include "../Stage.h"
#include "../Player.h"
//#include "Model.h"

/*	メモ　---------------------------------------------------

〇追跡
相手の位置捕捉
↓
自身の位置捕捉
↓
正面方向にレイを飛ばす
↓
相手へのベクトルと正面ベクトルから法線取得
↓
相手へのベクトル正面ベクトルから内積算出
↓
内積から自身と相手との角度計算
↓
法線を回転軸にして角度ぶん正面ベクトル回転
↓
移動(常に正面)

この中で壁があれば回避に移る流れを


〇回避フラグのON/OFF
相手の位置補足
↓
自身の位置補足
↓
相手と自身間の距離算出（�@）
↓
相手方向にレイ射出（当たる対象は壁）
↓
壁に当たった時のレイの距離と�@比較
↓
�@の方が短ければ（相手と自身間に壁がある）なら回避フラグON
�@の方が長ければ（相手と自身間に壁がない）なら回避フラグOFF

------------------------------------------------------------	*/

CharacterAI::CharacterAI()
	: isWallInRange_(false)
	, pGameObject_(nullptr), isFindTarget_(false) , isDeadEnd_(false)
	, speedRate_(0.f), avoidRange_(2.f), pStage_(nullptr), pPlayer_(nullptr)
	, OpponentData({ g_XMZero }), moveSpeed_(0), currentState_(AI_STATE::DEFAULT), angle_(0.f)
{
	//初期化
	minFrontDistToPlayer_ = 99.f;
	rayRecord_ = { g_XMZero, false };

	//ステージ情報(CSV)
	CsvReader csv;
	csv.Load("Assets/MapData.csv");
	StageData.x_ = (int)csv.GetWidth();
	StageData.z_ = (int)csv.GetHeight();

	//デバッグ用のベクトル
#ifdef  _DEBUG	
	hDebugModel_ = Model::Load("Assets/DebugVector.fbx", SHADER_DEBUG);
#endif //_DEBUG

}

//デストラクタ
CharacterAI::~CharacterAI() = default;

//AIの搭載オブジェクトセット
void CharacterAI::SetAiObject(GameObject * gameObject, float moveSpeed)
{
	pGameObject_ = gameObject;		//搭載オブジェクト
	moveSpeed_ = moveSpeed;			//移動速度
	//currentState_ = state;		//今のステート（実行するやつ）
}

void CharacterAI::SetState(AI_STATE state)
{
	currentState_ = state;
}

//対戦相手のデータをセット
//追記：アイテムの追加で追跡対象に優先順位を設ける
void CharacterAI::SetOpponentObject(GameObject * opponentObject)
{
	//必要に応じて随時この関数内で結ぶデータ追加
	OpponentData.pos = opponentObject->GetPosition();											//座標
	OpponentData.dir = XMVector3Normalize(OpponentData.pos - pGameObject_->GetPosition());		//方向
	OpponentData.dist = XMVector3Length(OpponentData.pos - pGameObject_->GetPosition()).vecX;	//距離
	OpponentData.priority = opponentObject->GetPriority();										//優先度
}

//レイの発射(常に最初に行う処理)
void CharacterAI::SetRayToAvoidTarget(GameObject * rayTarget)
{
	/* ---------------------------------------------------------------
	〇壁避けアルゴリズム案

	前方五方向にレイを発射(判定は常に更新して行う)
	↓
	正面方向のレイが範囲内に入ったら回避開始
	↓
	正面、斜め、真横の順序でレイを回転して確認
	↓
	範囲内に入っていないレイがあったらその方向に正面姿勢を回転
	=>回避が不十分だったのでプレイヤー方向と回避方向の和ベクトルに回転
	↓
	全レイが範囲内だったら範囲外に出るまで反転(行き止まりなので)
	↓
	壁を避け切ったら追跡再開(プレイヤー方向へ回転)

	(常に正面に移動している状態)

	----------------------------------------------------------------- */

	//ステージ登録
	pStage_ = (Stage*)rayTarget;
	assert(pStage_ != nullptr);

	//デバッグ用にプレイヤーも
	pPlayer_ = (Player*)rayTarget->FindObject("Player");
	assert(pPlayer_ != nullptr);

	//Transformの整理
	//Transform& trans = pGameObject_->transform_;

	//壁との距離
	float wallRayDist = 9999.f;

	//壁との最短距離
	float minWallDist = 9999.f;

	//デバッグ確認用
#ifdef _DEBUG
	XMVECTOR debugRayDir_[MAX_RAY_NUMBER] = { 0,0,0,0,0 };
#endif // _DEBUG

	//壁モデルの情報取得
	std::vector<int>* wallHandleList = pStage_->GetWallHandleList();

	//壁モデルのハンドル
	int hWall = pStage_->GetModelWallHandle();

	//回避方向を探すレイ構造体準備
	RayRecord rayToAvoid;

	//Stageのモデル毎の処理
	for (int z = 0; z < StageData.z_; z++)
	{
		for (int x = 0; x < StageData.x_; x++)
		{
			//壁でなければ無視
			if (pStage_->IsWall(x, z) == false)
				continue;

			//壁モデルのトランスフォーム
			Transform modelTransform;

			//座標合わせ
			modelTransform.position_ = XMVectorSet((float)x, 0.f, (float)z, 0.f);
			//modelTransform.Calclation();


			//Stageから受け取った壁のモデルハンドル全てにレイを発射 => hWallで指定した場合と変わらないのでナシ
			//for (auto itr = wallHandleList->begin(); itr != wallHandleList->end(); itr++)


			//AI付加オブジェクトのクォータニオン取得
			XMVECTOR quaternion = pGameObject_->GetQuaternion();
			XMVECTOR vector = pGameObject_->transform_.QuaternionToEuler(quaternion);
			
			
			//壁に向けてレイ発射(ここの結果がおかしいのでガビガビ)
			RayCast(hWall, &wallRayDist, &rayToAvoid, modelTransform, quaternion);

			//当たった距離が記録している最短距離より短いなら距離更新
			if (minWallDist > wallRayDist && (wallRayDist > 0))
				minWallDist = wallRayDist;

			//見つけたかどうか(目の前に壁があってかつプレイヤーと自身の間に壁がある)
			isFindTarget_ = (minWallDist > OpponentData.dist);

			//現在の最短距離が回避範囲内なら
			if (avoidRange_ > minWallDist)
			{
				//範囲内入ったフラグたて
				isWallInRange_ = true;

				//Playerの方が壁より近ければ追跡続行
				if (minWallDist > OpponentData.dist)
					return;

				//レイの回転
				//正面以外の方向へ回転させるので１スタート
				//todo Avoidでしか使わないので関数化したい
				for (int i = 1; i < MAX_RAY_NUMBER; i++)
				{
					//斜め45度ぶん(ラジアンで取得)
					float angle = XMConvertToRadians(30.f);

					//正面以外でこのループが偶数回目/奇数回目に応じて角度反転	
					//45度反転でなくさらに+45度加算
					angle *= (i % 2 == 0) ? 1 : -1;

					//正面とそれ以外の場合回転
					auto rotateY = XMQuaternionRotationAxis(g_XMIdentityR1, angle);
					//デバック(ブレークポインタ)確認用
					float angleEuler = XMConvertToDegrees(angle);

					//rotateYがゼロになってしまった場合
					if (XMQuaternionEqual(rotateY, g_XMZero))
					{
						std::string message = "rotateY";
						MessageBox(NULL, message.c_str(), "エラー", MB_OK);
					}

					//回転させたレイを再発射
					//ここのRayToAvoidは壁に向かって飛んでいるレイ
					RayCast(hWall, &wallRayDist, &rayToAvoid, modelTransform, rotateY);

					//回転させたレイが行けそうな道を見つけたら
					//↑そのレイを使って旋回する
					if (wallRayDist > (avoidRange_ * 1.1f))
					{
						//回避行動をとるため現時点のレイデータ記録
						//HitDataは「回避行動が決まった時の当たったレイデータ」
						rayRecord_ = rayToAvoid;
						break;
					}
					//最後まで行って回避先が見つからなければ行き止まり判定
					if (i == (MAX_RAY_NUMBER - 1))
						isDeadEnd_ = true;
				}



			}
			//回避完了判定(プレイヤーとの距離とプレイヤー方向の壁との距離を比較)
			//=>壁がプレイヤーの後ろにある（自身とプレイヤーの間に壁がない）
			if (OpponentData.dist <= minWallDist)
			{
				//フラグリセット（回避終了）
				isWallInRange_ = false;

				isDeadEnd_ = false;

			}

		}	//ステージモデル毎の処理
	}
}


//データを記録するレイキャスト
void CharacterAI::RayCast(int hundle, float* dist, RayRecord * rayRecord, Transform targetTrans, CXMVECTOR rayDir)
{
	/*  メモ
		ワールド座標への変換過程(Transform系統)が必要なのであれば追記
	*/

	//レイキャストのデータ
	RayCastData data;

	//レイの向いている方向(ここEnemyの向きに合わせるかも)
	//プレイヤーへの向きに
	//XMVECTOR rayFront = OpponentData.dir;
	XMVECTOR rayFront = g_XMIdentityR2;

	//発射位置(AIを付加したオブジェクト) 
	//data.start = XMVector3Transform(pGameObject_->GetPosition(), GetWorldMatrix());	//inWallRangeが連続でとられる
	data.start = pGameObject_->GetPosition();
	
	//方向設定(レイキャストを行うオブジェクトの向いている向き)
	data.dir = XMVector3Rotate(rayFront, rayDir);
	//data.dir = rayDir;

	//データ記録用のレイキャストの情報更新
	rayRecord->dir = data.dir;
	rayRecord->isHit = data.hit;

	//対象モデルのトランスフォームをセット
	Model::SetTransform(hundle, targetTrans);

	//レイキャスト
	Model::RayCast(hundle, &data);

	//当たったら
	if (data.hit)
	{
		//各レイデータの結びつけ		
		*dist = data.dist;
	}
}

//AIの実行
//タイミングはRayの後
void CharacterAI::Active()
{
	//今の状態のステートに合わせた動作を実行
	switch (currentState_)
	{
		//デフォルト状態
	case AI_STATE::DEFAULT:
		break;

		//逃避状態
	case AI_STATE::ESCAPE:
		break;

		//追跡状態
	case AI_STATE::TRACK_AND_AVOID:
		//壁の範囲外であれば
		if (!isWallInRange_)
			Track();
		else
			Avoid();
		break;

		//一時停止&再開
	case AI_STATE::STOP_AND_RESTART:
		//対象が見つかったら
		if (isFindTarget_)
			Track();
		else
			Search();
		break;

		//AI無効化
	case AI_STATE::INACTIVE:
		InActive();
		break;
	}
}

//AIの非実行(無効化)
void CharacterAI::InActive()
{
	moveSpeed_ = 0.f;
	//またはそれ用の関数準備
}

//追跡
void CharacterAI::Track()
{
	moveSpeed_ = 0.1f;

	//追う対象の位置
	XMVECTOR targetPos = OpponentData.pos;

	//Enemy視点のプレイヤーへのベクトル
	XMVECTOR targetToVec = OpponentData.dir;

	//プレイヤーとの距離
	//vecX,Y,Zいずれも数値は同じ
	float dist = XMVector3Length(targetToVec).vecX;

	//近づくにつれて減速(距離1に対して0.1)
	//=>大きさがあやふやなので要数値調整
	speedRate_ = pow((1.0f + 0.1f), dist) * moveSpeed_;

	//舵取り
	Rudder(targetToVec, targetToVec);
}

//回避
void CharacterAI::Avoid()
{
	//相手の位置ベクトルを一時ローカルに(多分後で書き換えるかも)
	XMVECTOR targetToVec = OpponentData.dir;

	/* 以下壁避けの回転 */

	//通過可能なレイ
	XMVECTOR passableDir_;

	//回避可能方向
	passableDir_ = rayRecord_.dir;

	//速度設定
	//speedRate_ = pow((1.0f - 0.01f), OpponentData.dist) * moveSpeed_;

	//どこにも行けない(例外の)場合、後退(反転旋回)
	//todo どこにも行けない場合(処理が甘い)
	if (!rayRecord_.isHit)
	{
#ifdef _DEBUG
		isDeadEnd_ = true;
#endif // _DEBUG

		//真横移動	
		auto angle = XMConvertToRadians(90); //90度ぶん(ラジアンで取得)
		//angle *= (OpponentData.vec.vecX > 0) ? -1 : 1;  //プレイヤーの方向に応じて旋回変化
		auto rotateY = XMQuaternionRotationAxis(g_XMIdentityR1, angle);
		passableDir_ = XMVector3Rotate(passableDir_, rotateY);

	}

	//デバッグ用
	//回避ベクトルの方向確認
#ifdef _DEBUG
	//std::cout << "DeadEnd : " << (isDeadEnd_ == false ? "FALSE" : "TRUE") << std::endl;
#endif // DEBUG

	//回避回転
	Rudder(targetToVec, passableDir_);
}

//探索
void CharacterAI::Search()
{
	/* -------------------------------------
	〇一時停止＆再開の流れ
	・エネミーの視点を回転(当たるまで)
	↓
	・視点がプレイヤーに当たる
	↓
	・追跡(向きもプレイヤーをトラッキング)
	↓
	・視点が途切れたら
	↓
	・一時停止
	=>移動量をAIヘッダで準備して操作
	↓
	・再度視点回転

	まず目の前に壁があるかどうか
	minFrontDistToPlayerは随時更新されるので、
	それでPlayerを見つけたかの判断に。あれば一時停止して回転
	----------------------------------------- */

	//計算合わせ
	//pGameObject_->TransCalclation();

	//正面ベクトル(Z)
	XMVECTOR front = g_XMIdentityR2;
	
	//回転クォータニオンを単位クォータニオンで準備(法線がない場合これで回転)
	auto rotateY = XMQuaternionIdentity();

	//回転度合増加
	angle_ += 1.f;
	if (angle_ == 360) angle_ = 0;	//一周で値リセット

	//角度をラジアンに変換
	float angle = XMConvertToRadians(angle_);

	//angleが0の場合法線が消えるので場合分け
	if (!XMVector3Equal(g_XMIdentityR1, g_XMZero))
		rotateY = XMQuaternionRotationAxis(g_XMIdentityR1, -angle);		//Y軸回転姿勢

	//frontの回転
	front = XMVector3Rotate(front, rotateY);

	//一時停止
	moveSpeed_ = 0.f;

	//オブジェクトの回転
	pGameObject_->transform_.quaternion_ = rotateY;

}

//舵取り
void CharacterAI::Rudder(CXMVECTOR targetVec, CXMVECTOR avoidVec)
{
	//正面ベクトル(今回は手前を正面に)
	XMVECTOR front = g_XMIdentityR2;

	//方角だけほしいので正規化
	XMVECTOR normalizeTargetVec = XMVector3Normalize(targetVec);
	XMVECTOR normalizeAvoidVec = XMVector3Normalize(avoidVec);

	//ベクトルの合成(回避のため)
	//XMVECTOR sumVec = normalizeTargetVec + normalizeAvoidVec;

	//正面からみた対象ベクトルへの角度を入れる変数
	float angle;

	//回避方向の有無に応じて旋回方向分岐(ここState差し替えたら変えるかも)
	isWallInRange_ == false ?
		angle = XMVector3AngleBetweenVectors(normalizeTargetVec, front).vecX :
		angle = XMVector3AngleBetweenVectors(normalizeAvoidVec, front).vecX;

	//法線(回転軸)生成
	XMVECTOR normal = XMVector3Cross(normalizeAvoidVec, front);
	normal.m128_f32[3] = 0;		//wは0に

	//回転クォータニオンを単位クォータニオンで準備(法線がない場合これで回転)
	auto rotateY = XMQuaternionIdentity();

	//angleが0の場合法線が消えるので場合分け
	if (!XMVector3Equal(normal, g_XMZero))
	{
		//Y軸回転姿勢(radian度回転済)
		rotateY = XMQuaternionRotationAxis(normal, -angle);
	}

	//正面ベクトルをrotateYで回転
	front = XMVector3Rotate(front, rotateY);

	//pGameの姿勢をfrontの向く方に
	pGameObject_->transform_.quaternion_ = rotateY;
	
	//移動ベクトル
	XMVECTOR move = (front * speedRate_);

	//移動
	//=>移動ベクトルを加算し、前の処理で移動ベクトルを回転する
	pGameObject_->transform_.position_ += move;

	//壁めり込み防止
	SaveDig((front * 0.1f));

}

//描画(デバッグ用)
#ifdef _DEBUG
void CharacterAI::Draw()
{
	Transform trans = pGameObject_->GetTransform();
	pGameObject_->GetPosition().vecY = 0.3f;
	pGameObject_->GetScale().vecZ = avoidRange_;
	//trans.Calclation();
	

	Model::SetTransform(hDebugModel_, trans);
	Model::Draw(hDebugModel_);
}
#endif // _DEBUG

//壁めり込み防止
void CharacterAI::SaveDig(CXMVECTOR move)
{
	const float WALL_SIZE = 0.4f;

	//チェック用の座標
	XMVECTOR cheakPos = pGameObject_->transform_.position_ - move;

	//座標チェック用の変数
	int cheakX, cheakZ;

	//上(奥)の判定
	cheakX = cheakPos.vecX;
	cheakZ = cheakPos.vecZ + WALL_SIZE;
	if (pStage_->IsWall(cheakX, cheakZ))
	{
		pGameObject_->transform_.position_.vecZ = cheakZ - WALL_SIZE;
	}

	//下(手前)の判定
	cheakX = cheakPos.vecX;
	cheakZ = cheakPos.vecZ - WALL_SIZE;
	if (pStage_->IsWall(cheakX, cheakZ))
	{
		pGameObject_->transform_.position_.vecZ = cheakZ + 1 + WALL_SIZE;
	}

	//左の判定
	cheakX = cheakPos.vecX - WALL_SIZE;
	cheakZ = cheakPos.vecZ;
	if (pStage_->IsWall(cheakX, cheakZ))
	{
		pGameObject_->transform_.position_.vecX = cheakX + 1 + WALL_SIZE;
	}

	//右の判定
	cheakX = cheakPos.vecX + WALL_SIZE;
	cheakZ = cheakPos.vecZ;
	if (pStage_->IsWall(cheakX, cheakZ))
	{
		pGameObject_->transform_.position_.vecX = cheakX - WALL_SIZE;
	}

}
