#pragma once
#include <list>
#include <vector>
#include "Transform.h"
#include "GameObject.h"
#include "Fbx.h"
//#include "Direct3D.h"
#include "Model.h"

class Stage;
class Player;

/* 主にEnemyのAI部分 */

//レイ番号(配列で使うのでenumで)
enum RAY_NUMBER
{
	FRONT = 0,
	LEFT_SKEW,
	RIGHT_SKEW,
	//LEFT,
	//RIGHT,
	MAX_RAY_NUMBER
};

//AIのステート
enum class AI_STATE : int
{
	DEFAULT = 0,
	ESCAPE,
	TRACK_AND_AVOID,
	STOP_AND_RESTART,
	INACTIVE,
};

class CharacterAI
{

private:

	//レイ記録構造体
	struct RayRecord
	{
		XMVECTOR dir;
		bool isHit;
		//float dist;
	};

	//対象(プレイヤー)の情報
	struct
	{
		XMVECTOR pos;		//位置
		XMVECTOR dir;		//方角
		float	 dist;		//距離
		int		 priority;	//優先度
		//float speed_;
		//あと必要に応じて追加
	}OpponentData;

	//ステージのデータ
	struct
	{
		int x_;
		int z_;
	}StageData;

	//レイ記録構造体用の変数
	RayRecord rayRecord_;

	//プレイヤー方向への壁に向けたレイの最小距離
	float minFrontDistToPlayer_;

	//壁とレイの距離を格納するリスト
	std::list<RayCastData*> disList_;

	//速度割合
	float speedRate_;

	//壁避けを開始する範囲
	const float avoidRange_;

	//ステージのポインタ
	Stage* pStage_;

	//プレイヤーのポインタ
	Player* pPlayer_;

	//AIを搭載するオブジェクト
	GameObject* pGameObject_;

	//AIを搭載するオブジェクトの移動速度
	float moveSpeed_;

	//実行するステート(状態)
	AI_STATE currentState_;

	//デバッグ用のベクトル表示のID
	int hDebugModel_;

	//壁が範囲内(正面レイから見て)にあったらtrue
	bool isWallInRange_;

	//見つけたかどうか
	bool isFindTarget_;

	//行き止まりかどうか
	bool isDeadEnd_;

	//角度
	float angle_;

public:

	//コンストラクタ
	CharacterAI();

	//デストラクタ
	virtual ~CharacterAI();

	//AIを搭載するオブジェクトをセット
	//引数：�@AIを搭載するオブジェクト、�Aオブジェクトの移動速度
	void SetAiObject(GameObject* gameObject, float moveSpeed);

	//ステートをセット(後で一つにする)
	void SetState(AI_STATE state);

	//対戦相手をセット
	//引数：対戦相手となるオブジェクト
	void SetOpponentObject(GameObject* opponentObject);

	//レイキャストのセット
	//引数：レイキャストを行う対象のオブジェクト(回避対象)
	void SetRayToAvoidTarget(GameObject* rayTarget);

	//引数で指定されたモデル(ハンドル)を対象にレイキャスト
	//引数：�@対象のモデルハンドル、�A対象とのの距離(受取用)、�B当たったレイのデータ(受取用)
	//      �C対象のモデルのトランスフォーム、�D回転姿勢(クォータニオン)
	void RayCast(int hundle, float* dist, RayRecord* rayRecord, Transform targetTrans, CXMVECTOR rayDir);

	//親のワールド行列の取得
	//戻値：親のワールド行列
	XMMATRIX GetWorldMatrix() { return pGameObject_->GetWorldMatrix(); }


	/* 以下ステートに基づいた動作処理 */


	//AIの行動実行	※これをAIをつけたオブジェクト先で呼び出す
	void Active();

	//AIの無効化
	void InActive();

	//舵取り
	//引数：�@対象へのベクトル、�A回避先のベクトル
	void Rudder(CXMVECTOR targetVec, CXMVECTOR avoidVec);

	//追跡
	void Track();

	//回避
	void Avoid();

	//探索
	void Search();

#ifdef _DEBUG
	//デバッグ時にベクトル表示
	void Draw();
#endif

	//壁めり込み防止
	void SaveDig(CXMVECTOR move);
};