#include "GameObject.h"
#include "SphereCollider.h"
#include "CharacterAI.h"

//引数なしのコンストラクタが呼ばれたら、引数ありのコンストラクタを初期化として呼ぶ(オーバーロード)
GameObject::GameObject() :GameObject(nullptr, "")
{
}

//コンストラクタ(標準)
GameObject::GameObject(GameObject * parent, const std::string & name)
	: pParent_(parent), objectName_(name), isDead_(false)
{
	childList_.clear();	//リストの初期化
}

//デストラクタ
GameObject::~GameObject()
{
	//子供以下削除
	KillAllChildren();
}

//子供以下すべて更新
void GameObject::UpdateSub()
{
	//自身の更新
	Update();

	//Transformの計算はUpdateの後〜Drawの前までに記述
	//=>Updeteの直後が適切？
	//Updateは当たり判定などもかかわる処理のため、計算はUpdate直後が適切(子は親についてくる)
	transform_.Calclation();

	//子リストの中身をすべてアップデート
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->UpdateSub();
	}
	for (auto itr = childList_.begin(); itr != childList_.end();)
	{
		//子リストの中身(各オブジェクト)が死んでいるか(フラグ確認)
		if ((*itr)->isDead_ == true)
		{
			//子の子たちをすべて消す
			(*itr)->ReleaseSub();
			//そのオブジェクトをdelete
			//SAFE_DELETEなどが必要な場合はGlobalヘッダなどを作成して使うとよい

			//deleteうんぬん
			//親クラスのポインタに子クラスの実態を入れることができる
			//=>その場合、その実態がdeleteされても呼ばれるのは親のデストラクタ
			//=>親デストラクタ自身にvirtualをつけることでdeleteの処理もオーバーライドされ、子のデストラクタが呼ばれる

			delete (*itr);

			//子供たちリストから削除
			//eraceの戻値で次のイテレーター指定(ここでリスト更新をしている)
			itr = childList_.erase(itr);

		}
		//eraceの更新がない場合、こちらで更新
		else
		{
			//当たり判定を呼ぶ必要アリ
			(*itr)->Collision(pParent_);

			itr++;
		}
	}

}

//子供以下すべて描画
void GameObject::DrawSub()
{
	//まずは自身のDraw関数を呼ぶ
	Draw();

	//子供たちのDraw関数をすべて呼ぶ
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->DrawSub();
	}
}

//子供以下すべて更新
void GameObject::ReleaseSub()
{
	Release();

	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->ReleaseSub();
	}

}

//オブジェクト探索
GameObject * GameObject::FindObject(std::string name)
{
	//名前で探索
	return GetRootjob()->FindChildObject(name);
}

//子供以下オブジェクト探索
GameObject * GameObject::FindChildObject(std::string name)
{
	//子供リストから検索
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		//名前が見つかったらそれを返却
		if ((*itr)->objectName_ == name)
		{
			return *itr;
		}

		//再起処理(ループ)
		GameObject *obj = (*itr)->FindChildObject(name);
		if (obj != nullptr)
		{
			return obj;
		}
	}
	//見つからなければnullptr
	return nullptr;
}

//最親取得
GameObject * GameObject::GetRootjob()
{
	if (pParent_ == nullptr)
	{
		return this;
	}
	else
	{
		//再起処理(ループ)
		return pParent_->GetRootjob();
	}
}

//オブジェクトの位置取得
XMVECTOR GameObject::GetPosition()
{
	return transform_.position_;
}

//オブジェクトの名前取得
const std::string & GameObject::GetObjectName(void) const
{
	return objectName_;
}

//コライダー追加
void GameObject::AddCollider(SphereCollider * collider)
{
	//コライダーをオブジェクトにセット
	collider->SetTargetObject(this);

	//リストにコライダー追加
	colliders_.push_back(collider);
}

//当たり判定をリストから行う
void GameObject::Collision(GameObject * pTarget)
{
	//自身の当たりはなし
	if (pTarget == nullptr || this == pTarget)
	{
		return;
	}

	//コライダーのリストから当たったものがあればOnCollision呼び出し
	//見るのは自身と対象
	for (auto i = this->colliders_.begin(); i != this->colliders_.end(); i++)
	{
		for (auto j = pTarget->colliders_.begin(); j != pTarget->colliders_.end(); j++)
		{
			if ((*i)->IsHit(*j))
			{
				this->OnCollision(pTarget);
			}
		}
	}

	//なければおわり
	if (pTarget->childList_.empty())
		return;

	//1つのオブジェクトが複数のコリジョン情報を持ってる場合もあるので二重ループ
	for (auto i = pTarget->childList_.begin(); i != pTarget->childList_.end(); i++)
	{
		Collision(*i);
	}

}


//AI搭載
//AI同士の対戦ができるようにGameObject型には付けられるようにする
void GameObject::AddCharacterAI(CharacterAI * ai, float moveSpeed)
{
	ai->SetAiObject(this, moveSpeed);
}


//死亡フラグon
void GameObject::KillMe()
{
	isDead_ = true;
}

//子供以下全消去
void GameObject::KillAllChildren()
{
	//自分の子供を全部消す
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->Release();
		delete(*itr);
	}
	childList_.clear();
}
