#pragma once
#include <d3d11.h>
#include <d3d11_1.h>

/* テキスト描画も行うのでDirect2Dもインクルード */
#include <d2d1.h>
#include <d2d1_1.h>	
#include <dwrite.h>	
#include <wchar.h>	

#include "Texture.h"



//リンカ
#pragma comment(lib, "d3d11.lib")	
#pragma comment(lib, "d3dcompiler.lib")	
#pragma comment(lib, "d2d1.lib")
#pragma comment(lib, "dwrite.lib")


//配列用の列挙(最後に仮最大値を追加することで要素が増えたときでも対応できる)
enum SHADER_TYPE { SHADER_3D, SHADER_2D, SHADER_PHONG, SHADER_TOON, SHADER_DEBUG, SHADER_MAX };


namespace Direct3D
{
	//変数のプロトタイプ宣言
	extern ID3D11Device*		pDevice;	
	extern ID3D11DeviceContext* pContext;
	extern int scrWidth;	//スクリーン幅
	extern int scrHeight;	//スクリーン高さ
	extern Texture* pToonTex;


	//初期化
	//引数：�@ウィンドウ幅、�Aウィンドウ高さ、�Bウィンドウハンドル
	//戻値：または E_FAIL
	HRESULT Initialize(int winW, int winH, HWND hWnd);

	//シェーダー準備
	HRESULT InitShader();

	//シェーダハンドルのセット
	void SetShaderHundle(SHADER_TYPE type);

	//3D用のシェーダ
	HRESULT InitSharder3D();

	//2D用のシェーダ
	HRESULT InitSharder2D();

	//テスト用のシェーダ(まっ平)
	HRESULT InitShaderTest();

	//トゥーンシェーダ
	HRESULT InitShaderToon();

	//デバッグ用シェーダ
	HRESULT InitShaderDebug();

	/* 取得関数系 */

	//スワップチェイン１を渡す
	IDXGISwapChain1* GetSwapChain1();

	//DXGIDeviceを渡す
	IDXGIDevice* GetIDXGIDevice();

	//DXGISurfaceを渡す
	IDXGISurface* GetIDXGISurface();

	//D2DDeviceContextを渡す
	ID2D1DeviceContext* GetD2DDeviceContext();

	//SolidBrushを渡す
	ID2D1SolidColorBrush* GetSolidBrush();

	//フォーマットを渡す
	IDWriteTextFormat* GetTextFormat();

	//テキストフォーマットの作成
	HRESULT CreateTextFormat(IDWriteTextFormat** ppTextFormat, float size);

	//描画開始
	void BeginDraw();

	//描画終了
	HRESULT EndDraw();

	//解放
	void Release();

};