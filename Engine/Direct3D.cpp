#include <d3dcompiler.h>
#include <assert.h>
#include <wrl/client.h>

#include "Direct3D.h"
#include "Camera.h"
#include "Global.h"


//変数
namespace Direct3D
{
	/* 3D用 */
	//クラス型の変数なのでポインタに(newの代わりのDirectX専用の関数)
	ID3D11Device*			pDevice = nullptr;			 //デバイス
	ID3D11DeviceContext*    pContext = nullptr;			 //デバイスコンテキスト
	IDXGISwapChain*         pSwapChain = nullptr;		 //スワップチェイン
	ID3D11RenderTargetView* pRenderTargetView = nullptr; //レンダーターゲットビュー
	ID3D11Texture2D*		pDepthStencil;				 //深度ステンシル(テクスチャでZバッファ準備)
	ID3D11DepthStencilView* pDepthStencilView;			 //深度ステンシルビュー

	/* 2D用 */
	IDWriteFactory*			pDWriteFactory = nullptr;	 //デバイス作成工場(?)
	ID2D1Factory1*			pD2DFactory1 = nullptr;		 //
	ID2D1DeviceContext*		pD2DDeviceContext = nullptr; //デバイスコンテキスト(2D)
	ID2D1Device*			pD2DDevice = nullptr;		 //デバイス(2D)
	ID2D1Bitmap1*			pBitmap = nullptr;			 //ビットマップ
	IDWriteTextFormat*		pTextFormat = nullptr;		 //テキストのフォーマット
	ID2D1SolidColorBrush*	pSolidBrush = nullptr;		 //ブラシ
	IDXGIDevice*			pDxgiDevice = nullptr;		 //
	IDXGISurface*			pSurface = nullptr;			 //
	IDXGISwapChain1*		pSwapChain1 = nullptr;		 //スワップチェイン

	//スクリーンの大きさ変数
	int scrWidth = 0;
	int scrHeight = 0;

	//Toon用の画像
	Texture* pToonTex;		

	//シェーダ情報を入れる構造体
	struct ShaderBundle
	{
		ID3D11VertexShader*		pVertexShader = nullptr;	//頂点シェーダー
		ID3D11PixelShader*		pPixelShader = nullptr;		//ピクセルシェーダー
		ID3D11InputLayout*		pVertexLayout = nullptr;	//頂点インプットレイアウト
		ID3D11RasterizerState*	pRasterizerState = nullptr;	//ラスタライザー
	};

	//2D用3D用で準備するため配列で同じものを二つ取得
	ShaderBundle shaderBundle[SHADER_MAX];
}

//初期化
HRESULT Direct3D::Initialize(int winW, int winH, HWND hWnd)
{
	//確認用変数
	HRESULT hr;

	/* まずは2D系作成 */
	{
		//ID3D型の未解放を知らせるフラグ
		UINT flags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

		//デバイス作成
		hr = D3D11CreateDevice(
			nullptr,
			D3D_DRIVER_TYPE_HARDWARE,
			nullptr,
			flags,
			nullptr, 
			0,
			D3D11_SDK_VERSION,
			&pDevice,
			nullptr,
			&pContext);
		CHECK_HRESULT(hr, "D3D11CreateDevice");

		//DXGI
		hr = pDevice->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&pDxgiDevice));
		CHECK_HRESULT(hr, "QueryInterface");

		//Adapter
		IDXGIAdapter* adapter = nullptr;
		hr = pDxgiDevice->GetAdapter(&adapter);
		CHECK_HRESULT(hr, "dxgiDevice");

		//Factory2の取得
		IDXGIFactory2* dxgiFactory = nullptr;
		hr = adapter->GetParent(__uuidof(IDXGIFactory2), reinterpret_cast<void**>(&dxgiFactory));
		CHECK_HRESULT(hr, "adapter");

		SAFE_RELEASE(adapter);


		//スワップチェインに与える情報
		DXGI_SWAP_CHAIN_DESC1 props = {};
		props.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		props.SampleDesc.Count = 1;
		props.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		props.BufferCount = 1;

		//スワップチェイン作成
		dxgiFactory->CreateSwapChainForHwnd(
			pDevice,
			hWnd,
			&props,
			nullptr,
			nullptr,
			&pSwapChain1);

		SAFE_RELEASE(dxgiFactory);

		//2DDevice作成の準備
		
		D2D1_FACTORY_OPTIONS factoryOptions = {};
#ifdef _DEBUG
		factoryOptions.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;
#endif
		//Direct2DのFactoryの作成
		D2D1CreateFactory(
			D2D1_FACTORY_TYPE_SINGLE_THREADED,
			factoryOptions,
			&pD2DFactory1);

		//デバイスの作成
		pD2DFactory1->CreateDevice(
			pDxgiDevice,
			&pD2DDevice
		);

		//デバイスコンテキストの作成
		pD2DDevice->CreateDeviceContext(
			D2D1_DEVICE_CONTEXT_OPTIONS_NONE,
			&pD2DDeviceContext);

		//デバイスコンテキストとスワップチェーンの接続
		pSwapChain1->GetBuffer(
			0,
			__uuidof(pSurface),
			reinterpret_cast<void**>(&pSurface));
	}


	/* 各種設定(3D用) */
	{
		//いろいろな設定項目をまとめた構造体
		DXGI_SWAP_CHAIN_DESC scDesc;

		//とりあえず全部0
		ZeroMemory(&scDesc, sizeof(scDesc));

		//描画先のフォーマット
		scDesc.BufferDesc.Width = winW;		//画面幅
		scDesc.BufferDesc.Height = winH;	//画面高さ
		scDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;	// 何色使えるか

		//FPS（1/60秒に1回）
		scDesc.BufferDesc.RefreshRate.Numerator = 60;
		scDesc.BufferDesc.RefreshRate.Denominator = 1;

		//その他
		scDesc.Windowed = TRUE;									//ウィンドウモードかフルスクリーンか
		scDesc.OutputWindow = hWnd;								//ウィンドウハンドル
		scDesc.BufferCount = 1;									//バックバッファの枚数
		scDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;	//バックバッファの使い道＝画面に描画するために
		scDesc.SampleDesc.Count = 1;							//MSAA（アンチエイリアス）の設定
		scDesc.SampleDesc.Quality = 0;							//　〃


		/* 上記設定をもとにデバイス、コンテキスト、スワップチェインを作成 */
		
		//スワップチェインは↑で作っているのでコメント化
		/*
		D3D_FEATURE_LEVEL level;
		D3D11CreateDeviceAndSwapChain(
			nullptr,					// どのビデオアダプタを使用するか？既定ならばnullptrで
			D3D_DRIVER_TYPE_HARDWARE,	// ドライバのタイプを渡す。ふつうはHARDWARE
			nullptr,					// 上記をD3D_DRIVER_TYPE_SOFTWAREに設定しないかぎりnullptr
			0,							// 何らかのフラグを指定する。（デバッグ時はD3D11_CREATE_DEVICE_DEBUG？）
			nullptr,					// デバイス、コンテキストのレベルを設定。nullptrにしとけばOK
			0,							// 上の引数でレベルを何個指定したか
			D3D11_SDK_VERSION,			// SDKのバージョン。必ずこの値
			&scDesc,					// 上でいろいろ設定した構造体
			&pSwapChain,				// 無事完成したSwapChainのアドレスが返ってくる
			&pDevice,					// 無事完成したDeviceアドレスが返ってくる
			&level,						// 無事完成したDevice、Contextのレベルが返ってくる
			&pContext);					// 無事完成したContextのアドレスが返ってくる
		*/

		/* レンダーターゲットビュー作成 */

		//スワップチェーンからバックバッファを取得（バックバッファ ＝ レンダーターゲット）
		ID3D11Texture2D* pBackBuffer;
		pSwapChain1->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);

		//レンダーターゲットビューを作成
		hr = pDevice->CreateRenderTargetView(pBackBuffer, NULL, &pRenderTargetView);
		CHECK_HRESULT(hr, "レンダービュー作成失敗");

		//一時的にバックバッファを取得しただけなので解放
		pBackBuffer->Release();


		/* ビューポート（描画範囲）設定 */

		//レンダリング結果を表示する範囲
		D3D11_VIEWPORT vp;
		vp.Width = (float)winW;		//幅
		vp.Height = (float)winH;	//高さ
		vp.MinDepth = 0.0f;			//手前
		vp.MaxDepth = 1.0f;			//奥
		vp.TopLeftX = 0;			//左
		vp.TopLeftY = 0;			//上

		//深度ステンシルビューの作成
		D3D11_TEXTURE2D_DESC descDepth;
		descDepth.Width = winW;
		descDepth.Height = winH;
		descDepth.MipLevels = 1;
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT_D32_FLOAT;
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;
		pDevice->CreateTexture2D(&descDepth, NULL, &pDepthStencil);
		pDevice->CreateDepthStencilView(pDepthStencil, NULL, &pDepthStencilView);

		//データを画面に描画するための一通りの設定（パイプライン）
		pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);  // データの入力種類を指定
		pContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);   // 描画先を設定
		pContext->RSSetViewports(1, &vp);

		//シェーダー準備
		CHECK_HRESULT(InitShader(), "シェーダ準備失敗");

		//カメラ準備
		Camera::Initialize();

		//スクリーンの大きさを結び付け
		scrWidth = winW;
		scrHeight = winH;
	}


	/* Direct2Dの準備 */
	{
		auto propsBit = D2D1::BitmapProperties1(
			D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
			D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE));

		pD2DDeviceContext->CreateBitmapFromDxgiSurface(
			pSurface,
			propsBit,
			&pBitmap);

		//Deviceの書込先をBitMapに指定
		pD2DDeviceContext->SetTarget(pBitmap);

		FLOAT dpiX = (FLOAT)GetDpiForWindow(GetDesktopWindow());
		FLOAT dpiY = dpiX;
		//pD2DFactory1->GetDesktopDpi(&dpiX, &dpiY);	//vs2019では合わなかったのでコメント化
		pD2DDeviceContext->SetDpi(dpiX, dpiY);

		D2D1_RENDER_TARGET_PROPERTIES properties =
			D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT,
				D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED), dpiX, dpiY);

		hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown**>(&pDWriteFactory));
		CHECK_HRESULT(hr, "DWriteCreateFactory");

		//テキストフォーマットの設定 ※メモ参照
		//引数：�@フォント名、�Aフォントコレクション、�B太さ、�Cスタイル、
		//      �D幅、�Eサイズ、�Fロケール名(?)、�Gテキストフォーマット
		//hr = pDWriteFactory->CreateTextFormat(L"メイリオ", nullptr, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 20, L"", &pTextFormat);
		hr = CreateTextFormat(&pTextFormat, 30);
		CHECK_HRESULT(hr, "pDWriteFactory");

		//テキスト位置の設定
		//引数：テキストの配置（前：LEADING, 後：TRAILING, 中央：CENTER）
		//hr = pTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
		//CHECK_HRESULT(hr, "pTextFormat");

		//テキスト色の設定
		//引数：フォント色
		hr = pD2DDeviceContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Black), &pSolidBrush);
		CHECK_HRESULT(hr, "pD2DDeviceContext");

	}

	return S_OK;
}


//シェーダー準備
HRESULT Direct3D::InitShader()
{

	//const,&は使用せずそのまま関数として呼び出して使用
	CHECK_HRESULT(InitSharder3D(), "3Dシェーダ準備失敗");
	CHECK_HRESULT(InitSharder2D(), "2Dシェーダ準備失敗");
	CHECK_HRESULT(InitShaderTest(), "テストシェーダ準備失敗");
	CHECK_HRESULT(InitShaderToon(), "トゥーンシェーダ準備失敗");
	CHECK_HRESULT(InitShaderDebug(), "デバッグシェーダ準備失敗");

	//それぞれをデバイスコンテキストにセット
	//=>シェーダの選択は読み込み先のLoadにて
	SetShaderHundle(SHADER_2D);
	SetShaderHundle(SHADER_3D);
	SetShaderHundle(SHADER_PHONG);
	SetShaderHundle(SHADER_TOON);
	SetShaderHundle(SHADER_DEBUG);

	return S_OK;
}

//シェーダハンドル準備
void Direct3D::SetShaderHundle(SHADER_TYPE type)
{
	pContext->VSSetShader(shaderBundle[type].pVertexShader, NULL, 0);	//頂点シェーダー
	pContext->PSSetShader(shaderBundle[type].pPixelShader, NULL, 0);	//ピクセルシェーダー
	pContext->IASetInputLayout(shaderBundle[type].pVertexLayout);		//頂点インプットレイアウト
	pContext->RSSetState(shaderBundle[type].pRasterizerState);			//ラスタライザー
}

//3D
HRESULT Direct3D::InitSharder3D()
{
	DWORD vectorSize = sizeof(XMVECTOR);

	// 頂点シェーダの作成（コンパイル）
	// 物体を画面のどこに表示するのかを決める機能
	ID3DBlob *pCompileVS = nullptr;
	//ここでSimple3Dを使用、その中のVSという関数を使うよう指示している
	D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL);
	assert(pCompileVS != nullptr);
	CHECK_HRESULT(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle[SHADER_3D].pVertexShader), "頂点シェーダ作成失敗");

	// 頂点インプットレイアウト(頂点がもつ位置や色などの情報)
	// 頂点にどんな情報を持たせるか
	D3D11_INPUT_ELEMENT_DESC layout[] = 
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,						D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, vectorSize ,		D3D11_INPUT_PER_VERTEX_DATA, 0 },	//UV座標
		{ "NORMAL",	  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },	//法線
	};

	CHECK_HRESULT(pDevice->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_3D].pVertexLayout),
		"頂点インプットレイアウト失敗");

	pCompileVS->Release();

	// ピクセルシェーダの作成（コンパイル）
	// ラスタライザで求められた表示ピクセルを何色にするか、影などの要素を加えて表示される最終的な色を決定する
	ID3DBlob *pCompilePS = nullptr;
	D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
	assert(pCompilePS != nullptr);
	CHECK_HRESULT(pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle[SHADER_3D].pPixelShader), "ピクセルシェーダ作成失敗");
	
	pCompilePS->Release();


	//ラスタライザ作成
	//どのピクセルを光らせるのかを決定
	//2Dであってもこのまま流用
	D3D11_RASTERIZER_DESC rdc = {};
	rdc.CullMode = D3D11_CULL_BACK;		//表示する必要のない面の非表示(今回は裏面)
	rdc.FillMode = D3D11_FILL_SOLID;	//表面を描画(線だけの場合もある)
	rdc.FrontCounterClockwise = FALSE;	//裏表の決定(通常は時計回りなら表)
	CHECK_HRESULT(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_3D].pRasterizerState), "ラスタライザ作成失敗");

	return S_OK;
}

//2D
//以下記述の関数とほぼ同じ
HRESULT Direct3D::InitSharder2D()
{
	DWORD vectorSize = sizeof(XMVECTOR);

	// 頂点シェーダの作成（コンパイル）
	ID3DBlob *pCompileVS = nullptr;
	//ここでSimple3Dを使用、その中のVSという関数を使うよう指示している
	D3DCompileFromFile(L"Simple2D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL);
	assert(pCompileVS != nullptr);
	CHECK_HRESULT(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle[SHADER_2D].pVertexShader), "頂点シェーダの作成失敗");

	// 頂点インプットレイアウト(頂点がもつ位置や色などの情報)
	D3D11_INPUT_ELEMENT_DESC layout[] = 
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,					D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, vectorSize ,		D3D11_INPUT_PER_VERTEX_DATA, 0 },	//UV座標
		{ "NORMAL",	  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },	//法線
	};
	CHECK_HRESULT(pDevice->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_2D].pVertexLayout), "頂点インプットレイアウト失敗");

	pCompileVS->Release();

	// ピクセルシェーダの作成（コンパイル）
	ID3DBlob *pCompilePS = nullptr;
	D3DCompileFromFile(L"Simple2D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
	assert(pCompilePS != nullptr);
	CHECK_HRESULT(pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle[SHADER_2D].pPixelShader), "ピクセルシェーダの作成失敗");

	pCompilePS->Release();

	//ラスタライザ作成
	D3D11_RASTERIZER_DESC rdc = {};
	rdc.CullMode = D3D11_CULL_BACK;		//表示する必要のない面の非表示(今回は裏面)
	rdc.FillMode = D3D11_FILL_SOLID;	//表面を描画(線だけの場合もある)
	rdc.FrontCounterClockwise = FALSE;	//裏表の決定(通常は時計回りなら表)
	CHECK_HRESULT(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_2D].pRasterizerState), "ラスタライザの作成失敗");

	return S_OK;
}

//テスト用のシェーダ
HRESULT Direct3D::InitShaderTest()
{
	/* ここでhlslとがっちり結び付け */

	DWORD vectorSize = sizeof(XMVECTOR);

	// 頂点シェーダの作成（コンパイル）
	ID3DBlob *pCompileVS = nullptr;
	D3DCompileFromFile(L"TestShader.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL);
	CHECK_HRESULT(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle[SHADER_PHONG].pVertexShader), "頂点シェーダ作成失敗");

	// ピクセルシェーダの作成（コンパイル）
	ID3DBlob* pCompilePS = nullptr;
	D3DCompileFromFile(L"TestShader.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
	CHECK_HRESULT(pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle[SHADER_PHONG].pPixelShader), "ピクセルシェーダ作成失敗");

	//頂点インプットレイアウト
	//頂点情報としてどんな情報を扱うかの指定
	D3D11_INPUT_ELEMENT_DESC layout[] = 
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,					D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, vectorSize,		D3D11_INPUT_PER_VERTEX_DATA, 0 },	//UV座標
		{ "NORMAL",	  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },	//法線
	};
	CHECK_HRESULT(pDevice->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_PHONG].pVertexLayout), "頂点インプットレイアウト失敗");
	
	pCompileVS->Release();	
	pCompilePS->Release();


	//ラスタライザ作成
	D3D11_RASTERIZER_DESC rdc = {};
	rdc.CullMode = D3D11_CULL_BACK;
	rdc.FillMode = D3D11_FILL_SOLID;
	rdc.FrontCounterClockwise = TRUE;
	CHECK_HRESULT(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_PHONG].pRasterizerState), "ラスタライザ作成失敗");

	return S_OK;
}

//トゥーンシェーダ
HRESULT Direct3D::InitShaderToon()
{
	pToonTex = new Texture;
	pToonTex->Load("Assets/toon.png");

	DWORD vectorSize = sizeof(XMVECTOR);

	// 頂点シェーダの作成（コンパイル）
	ID3DBlob *pCompileVS = nullptr;
	D3DCompileFromFile(L"ToonShader.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL);
	CHECK_HRESULT(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle[SHADER_TOON].pVertexShader),"頂点シェーダ作成失敗");	

	//頂点インプットレイアウト
	D3D11_INPUT_ELEMENT_DESC layout[] = 
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,					D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, vectorSize ,		D3D11_INPUT_PER_VERTEX_DATA, 0 },	//UV座標
		{ "NORMAL",	  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },	//法線
	};
	CHECK_HRESULT(pDevice->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_TOON].pVertexLayout), "頂点インプットレイアウト失敗");

	pCompileVS->Release();

	// ピクセルシェーダの作成（コンパイル）
	ID3DBlob *pCompilePS = nullptr;
	D3DCompileFromFile(L"ToonShader.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
	CHECK_HRESULT(pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle[SHADER_TOON].pPixelShader), "ピクセルシェーダ作成失敗");
	
	pCompilePS->Release();


	//ラスタライザ作成
	D3D11_RASTERIZER_DESC rdc = {};
	rdc.CullMode = D3D11_CULL_BACK;
	rdc.FillMode = D3D11_FILL_SOLID;
	rdc.FrontCounterClockwise = TRUE;
	CHECK_HRESULT(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_TOON].pRasterizerState), "ラスタライザ作成失敗");
	
	return S_OK;
}

//デバッグシェーダ
HRESULT Direct3D::InitShaderDebug()
{
	// 頂点シェーダの作成（コンパイル）
	ID3DBlob *pCompileVS = nullptr;
	D3DCompileFromFile(L"DebugShader.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL);
	CHECK_HRESULT(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle[SHADER_DEBUG].pVertexShader), "頂点シェーダ作成失敗");

	//頂点インプットレイアウト
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
	};
	CHECK_HRESULT(pDevice->CreateInputLayout(layout, 1, pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_DEBUG].pVertexLayout), "頂点インプットレイアウト失敗");

	pCompileVS->Release();

	// ピクセルシェーダの作成（コンパイル）
	ID3DBlob *pCompilePS = nullptr;
	D3DCompileFromFile(L"DebugShader.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
	CHECK_HRESULT(pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle[SHADER_DEBUG].pPixelShader), "ピクセルシェーダ作成失敗");

	pCompilePS->Release();


	//ラスタライザ作成
	D3D11_RASTERIZER_DESC rdc = {};
	rdc.CullMode = D3D11_CULL_BACK;
	rdc.FillMode = D3D11_FILL_WIREFRAME;
	rdc.FrontCounterClockwise = TRUE;
	CHECK_HRESULT(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_DEBUG].pRasterizerState), "ラスタライザ作成失敗");

	return S_OK;
}


/* 各クラス型変数取得関数 */

IDXGISwapChain1* Direct3D::GetSwapChain1()
{
	return pSwapChain1;
}

IDXGIDevice* Direct3D::GetIDXGIDevice()
{
	return pDxgiDevice;
}

IDXGISurface* Direct3D::GetIDXGISurface()
{
	return pSurface;
}

ID2D1DeviceContext* Direct3D::GetD2DDeviceContext()
{
	return pD2DDeviceContext;
}

ID2D1SolidColorBrush* Direct3D::GetSolidBrush()
{
	return pSolidBrush;
}

IDWriteTextFormat* Direct3D::GetTextFormat()
{
	return pTextFormat;
}

//テキストフォーマットの作成
HRESULT Direct3D::CreateTextFormat(IDWriteTextFormat** ppTextFormat, float size)
{
	HRESULT hr;

	//前のテキストフォーマットの開放
	SAFE_RELEASE((*ppTextFormat));

	//todo フォント等の設定も引数で操作可能に
	hr = pDWriteFactory->CreateTextFormat(
		L"メイリオ", nullptr, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, size, L"", ppTextFormat);
	CHECK_HRESULT(hr, "CreateTextFormat");

	//テキストの配置
	hr = (*ppTextFormat)->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
	CHECK_HRESULT(hr, "CreateTextFormat SetText");

	return S_OK;
}


//描画開始
void Direct3D::BeginDraw()
{
	//背景の色
	float clearColor[4] = { 0.f, 0.3f, 0.7f, 1.0f };	//R,G,B,A

	//画面をクリア
	pContext->ClearRenderTargetView(pRenderTargetView, clearColor);

	//カメラ処理
	Camera::Update();

	//深度バッファクリア
	pContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	//2D(テキスト)の描画
	pD2DDeviceContext->BeginDraw();

}

//描画終了
HRESULT Direct3D::EndDraw()
{
	pD2DDeviceContext->EndDraw();
	
	//スワップ（バックバッファを表に表示する）
	//CHECK_HRESULT(pSwapChain->Present(0, 0), "スワップ失敗");
	CHECK_HRESULT(pSwapChain1->Present(0, 0), "スワップ失敗");	//2Dで準備したものを

	return S_OK;
}

//解放処理
void Direct3D::Release()
{
	/* 2D */
	SAFE_RELEASE(pSolidBrush);
	SAFE_RELEASE(pTextFormat);
	SAFE_RELEASE(pDWriteFactory);
	SAFE_RELEASE(pBitmap);
	SAFE_RELEASE(pD2DFactory1);
	SAFE_RELEASE(pD2DDeviceContext);
	SAFE_RELEASE(pD2DDevice);

	SAFE_RELEASE(pDxgiDevice);
	SAFE_RELEASE(pSwapChain1);
	SAFE_RELEASE(pSurface);


	/* 3D */
	SAFE_RELEASE(pToonTex);
	SAFE_DELETE(pToonTex);

	//解放処理(Releaseはタイミング・作った逆順に)
	for (int i = 0; i < SHADER_MAX; i++) {
		SAFE_RELEASE(shaderBundle[i].pRasterizerState);
		SAFE_RELEASE(shaderBundle[i].pVertexLayout);
		SAFE_RELEASE(shaderBundle[i].pPixelShader);
		SAFE_RELEASE(shaderBundle[i].pVertexShader);
	}

	SAFE_RELEASE(pRenderTargetView);
	SAFE_RELEASE(pSwapChain);
	SAFE_RELEASE(pContext);
	SAFE_RELEASE(pDevice);
}

/*  メモ
* 　CreateTextFormat()の引数は以下サイト参考
* 　�B太さ　https://docs.microsoft.com/ja-jp/windows/win32/api/dwrite/ne-dwrite-dwrite_font_weight
* 　�Cスタイル　https://docs.microsoft.com/en-us/windows/win32/api/dwrite/ne-dwrite-dwrite_font_style
* 　�D幅　https://docs.microsoft.com/en-us/windows/win32/api/dwrite/ne-dwrite-dwrite_font_stretch
* 
*   todo D3D D2Dを一律スマートポインタ化
*/