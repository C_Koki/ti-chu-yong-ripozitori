#include "Math.h"

//行列式
float Math::Det(XMVECTOR a, XMVECTOR b, XMVECTOR c)
{
	return	(a.vecX * b.vecY * c.vecZ) +
		(a.vecY * b.vecZ * c.vecX) +
		(a.vecZ * b.vecX * c.vecY) -
		(a.vecX * b.vecZ * c.vecY) -
		(a.vecY * b.vecX * c.vecZ) -
		(a.vecZ * b.vecY * c.vecX);
}

//レイと三角形の交差判定
bool Math::Intersect(CXMVECTOR origin, CXMVECTOR ray, CXMVECTOR v0, CXMVECTOR v1, CXMVECTOR v2, float* distance)
{
	//クラメルの公式から、レイと三角形が当たっているかどうかを判定	
	//クラメルの公式：https://www.google.com/url?q=https%3A%2F%2Fshikousakugo.wordpress.com%2F2015%2F11%2F22%2Fcramers-rule%2F&sa=D&sntz=1&usg=AFQjCNEu3ISOvGGyYIGxP-NPcURzbLemvA
	//				  origin + ray * t = v0 + edge1 * u + edge2 * v

	//rayは単位ベクトル、不安であれば正規化して計算するのがよい
	XMVECTOR dir = XMVector3Normalize(ray);

	XMVECTOR edge1 = v1 - v0;
	XMVECTOR edge2 = v2 - v0;


	float denom = Det(edge1, edge2, -dir);

	//平行方向のレイだった場合、demonはマイナスになる
	if (denom < 0)
	{
		return false;
	}

	float numU = Det(origin - v0, edge2, -dir);
	float numV = Det(edge1, origin - v0, -dir);

	//この後にポリゴン計算などの重い計算がある場合は、この時点で分岐処理を行うほうが
	//後の計算をしなくてよい

	float u = numU / denom;
	float v = numV / denom;

	//ポインタを引数にすることで、受け取ったアドレスに直接入れることになる
	*distance = Det(edge1, edge2, origin - v0) / denom;

	//条件を満たしているのであればtrue、そうでなければfalse
	//if文は重い処理なんで、削れるものは削ったほうがいい
	if (0 <= u && 0 <= v && (u + v) <= 1 && distance >= 0)
	{
		return true;
	}
	return false;

}

//線形補間
float Math::Lerp(float x, float y, float s)
{
	return x + s * (y - x);
}

//ポテンシャル関数取得
float Math::GetPotensial(PotentialData param)
{
	return -(param.attract_ / (float)pow(param.dist_, param.decayAttract_)) 
		+ (param.repuls_ / (float)pow(param.dist_, param.decayRepuls_));
}

/*  メモ
　いずれの三角形交点の計算が上手くいかなかったので現在はDirectXのものを使用中。
　再度確認して使えそうなのであれば自作計算を使う
*/