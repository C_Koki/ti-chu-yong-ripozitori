#pragma once
#include <d3d11.h>
#include "string"

class Texture
{
	ID3D11SamplerState*	pSampler_;	//サンプラー作成
	ID3D11ShaderResourceView*	pSRV_;	//シェーダーソースビュー作成
	UINT imgWidth_;   //unsigned intの略
	UINT imgHeight_;

public:
	Texture();
	~Texture();
	HRESULT Load(std::string fileName);
	void Release();

	//ゲッター(外部から見たい情報のため)
	ID3D11SamplerState* GetSampler() { return pSampler_; }
	ID3D11ShaderResourceView* GetSRV() { return pSRV_; }
	UINT GetWidth() { return imgWidth_; }
	UINT GetHeight() { return imgHeight_; }

};