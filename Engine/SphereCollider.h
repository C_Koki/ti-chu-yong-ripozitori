#pragma once
#include <DirectXMath.h>
#include "GameObject.h"
using namespace DirectX;

/* 後で基底クラスとしてColliderクラスから継承するようにする */

class SphereCollider
{
protected:
	//コライダーをつけるオブジェクト
	GameObject* pGameObject_;

	//オブジェクトの位置(中心)
	XMVECTOR position_;

	//オブジェクトのサイズ
	XMVECTOR size_;

public:
	//コンストラクタ
	//引数：�@当たり判定の位置
	//		�A半径
	SphereCollider(XMVECTOR position, float radius);

	//デストラクタ
	virtual ~SphereCollider();

	//対象のオブジェクトをセット
	//引数：対象のオブジェクト
	void SetTargetObject(GameObject* gameObject);

	//衝突
	//引数：�@自身のオブジェクト
	//		�A衝突相手のオブジェクト
	bool HitTargetObject(SphereCollider* object, SphereCollider* target);

	//衝突判定
	//引数：衝突相手のオブジェクト
	bool IsHit(SphereCollider* target);

};
