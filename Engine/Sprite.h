#pragma once

#include <DirectXMath.h>
#include "Texture.h"
#include "Transform.h"

using namespace DirectX;

class Sprite
{
	//コンスタントバッファー
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matW;
	};

	//頂点情報
	struct VERTEX
	{
		XMVECTOR position;	//位置とUVで1セット
		XMVECTOR uv;
	};

//protected … privateと異なり継承先でも書き換えることができる
protected:	
	//変数(ポインタ)は後でコンストラクタで初期化
	ID3D11Buffer*	pVertexBuffer_;		//頂点バッファ
	ID3D11Buffer*	pIndexBuffer_;		//インデックスバッファ
	ID3D11Buffer*	pConstantBuffer_;	//コンスタントバッファ
	Texture*		pTexture_;			//テクスチャのポインタ
	int				indexNum;			//インデックス情報の数

public:
	Sprite();
	virtual ~Sprite();
	virtual HRESULT Initialize(std::string Name);	//名前をMainで書けるように引数をstring
	void	Draw(Transform& transform);				//移動回転縮小できるように引数をtransform
	void	Release();
};