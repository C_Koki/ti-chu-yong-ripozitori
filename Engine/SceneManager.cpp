#include "SceneManager.h"
#include "Model.h"
#include "../PlayScene.h"
#include "../PrePlayScene.h"


SceneManager::SceneManager()
{
}

SceneManager::SceneManager(GameObject * parent)
	: GameObject(parent, "SceneManager")
{
}

SceneManager::~SceneManager()
{
}

//シーン切り替え(中身を次シーンに)
void SceneManager::ChangeScene(SCENE_ID nextID)
{
	nextSceneID_ = nextID;
}

void SceneManager::Initialize()
{
	//最初のシーン登録 & 実装
	currentSceneID_ = SCENE_ID::PREPLAY;
	nextSceneID_ = currentSceneID_;
	Instantiate<PrePlayScene>(this);
}

void SceneManager::Update()
{
	//シーンが切り替わったら
	if (currentSceneID_ != nextSceneID_)
	{
		//子供以下全消去
		KillAllChildren();
		//モデル全解放
		Model::AllRelease();

		//nextSceneIDの中身に応じてシーン切り替え
		switch (nextSceneID_)
		{
		case SCENE_ID::PREPLAY: Instantiate<PrePlayScene>(this); break;
		case SCENE_ID::PLAY: Instantiate<PlayScene>(this); break;
		}
		//現在のシーン登録
		currentSceneID_ = nextSceneID_;
	}


}

void SceneManager::Draw()
{
}

void SceneManager::Release()
{
}
