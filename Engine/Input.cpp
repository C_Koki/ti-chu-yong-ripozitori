#include "Input.h"
#include "Global.h"

namespace Input
{
	//DirectInputのクラス
	LPDIRECTINPUT8 pDInput_ = nullptr;
	//ここでのデバイスはキーボードやマウスなど
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr;
	//キーボードの数、256は超えないとおもわれるので256
	BYTE keyState_[256] = { 0 };
	//前フレームでの各キーの状態
	BYTE prevKeyState_[256];    

	/* マウス */
	//マウス
	LPDIRECTINPUTDEVICE8	pMouseDevice_;		//デバイスオブジェクト
	DIMOUSESTATE			mouseState_;		//マウスの状態
	DIMOUSESTATE			prevMouseState_;	//前フレームのマウスの状態
	XMVECTOR				mousePos;			//マウスの位置

	/* コントローラー */
	//コントローラーの最大数
	const int MAX_PAD_NUM = 4;
	//コントローラーの状態を入れる変数
	XINPUT_STATE controllerState_[MAX_PAD_NUM];
	//前フレームのコントローラーの状態を変数
	XINPUT_STATE prevControllerState_[MAX_PAD_NUM];

	//初期化
	HRESULT Initialize(HWND hWnd_)
	{
		HRESULT hr;

		hr = DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&pDInput_, nullptr);
		CHECK_HRESULT(hr, "DirectInputの生成失敗");

		//ここでデバイスの種類（ここではキーボード）
		hr = pDInput_->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr);
		CHECK_HRESULT(hr, "デバイスオブジェクト生成失敗");

		//強調レベル（他の実行中のアプリに対する優先度）
		pKeyDevice->SetDataFormat(&c_dfDIKeyboard);
		pKeyDevice->SetCooperativeLevel(hWnd_, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);

		//マウス
		hr = pDInput_->CreateDevice(GUID_SysMouse, &pMouseDevice_, nullptr);
		CHECK_HRESULT(hr, "デバイスインプットの作成失敗");
		pMouseDevice_->SetDataFormat(&c_dfDIMouse);
		pMouseDevice_->SetCooperativeLevel(hWnd_, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);

		return S_OK;
	}

	//更新
	void Update()
	{
		//コピー先,コピー元,コピー先のサイズ
		memcpy(prevKeyState_, (void*)keyState_, sizeof(prevKeyState_));
		//キーボードを探す（仕様）
		pKeyDevice->Acquire();
		//その瞬間の全キーの状態を配列に入れる
		pKeyDevice->GetDeviceState(sizeof(keyState_), &keyState_);

		//マウス
		pMouseDevice_->Acquire();
		memcpy(&prevMouseState_, &mouseState_, sizeof(mouseState_));
		pMouseDevice_->GetDeviceState(sizeof(mouseState_), &mouseState_);

		//コントローラー
		for (int i = 0; i < MAX_PAD_NUM; i++)
		{
			memcpy(&prevControllerState_[i], &controllerState_[i], sizeof(controllerState_[i]));
			//複数使わないなら、1
			//今のコントローラーの状態を変数に入れる
			XInputGetState(i, &controllerState_[i]);
		}
	}

	//キーが押されている
	bool IsKey(int keyCode)
	{
		if (keyState_[keyCode] & 0x80)
		{
			return true;
		}

		return false;
	}

	//キーが押され瞬間
	bool IsKeyDown(int keyCode)
	{
		//今は押してて、前回は押してない
		if (IsKey(keyCode) && !(prevKeyState_[keyCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	//キーを放した瞬間
	bool IsKeyUp(int keyCode)
	{
		//今押してなくて、前回は押してる
		if (!IsKey(keyCode) && prevKeyState_[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	/*	マウス情報取得　*/

	//マウスのボタンが押されているか調べる
	bool IsMouseButton(int buttonCode)
	{
		//押してる
		if (mouseState_.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今押したか調べる（押しっぱなしは無効）
	bool IsMouseButtonDown(int buttonCode)
	{
		//今は押してて、前回は押してない
		if (IsMouseButton(buttonCode) && !(prevMouseState_.rgbButtons[buttonCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今放したか調べる
	bool IsMouseButtonUp(int buttonCode)
	{
		//今押してなくて、前回は押してる
		if (!IsMouseButton(buttonCode) && prevMouseState_.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスカーソルの位置を取得
	XMVECTOR GetMousePosition()
	{
		return mousePos;
	}

	//マウスの座標を設定
	void SetMousePosition(int x, int y)
	{
		mousePos = XMVectorSet((float)x, (float)y, 0.0f, 0.0f);
	}

	//そのフレームでのマウスの移動量を取得
	XMVECTOR GetMouseMove()
	{
		XMVECTOR result = XMVectorSet((float)mouseState_.lX, (float)mouseState_.lY, (float)mouseState_.lZ, 0);
		return result;
	}

	//マウスホイールの移動量
	float GetMouseWheel()
	{
		return (float)mouseState_.lZ;
	}

	/*  コントローラー情報取得  */

	//コントローラーのボタンが押されているか調べる
	bool IsPadButton(int buttonCode, int padID)
	{
		if (controllerState_[padID].Gamepad.wButtons & buttonCode)
		{
			return true; //押してる
		}
		return false; //押してない
	}

	//コントローラーのボタンを今押したか調べる（押しっぱなしは無効）
	bool IsPadButtonDown(int buttonCode, int padID)
	{
		//今は押してて、前回は押してない
		if (IsPadButton(buttonCode, padID) && !(prevControllerState_[padID].Gamepad.wButtons & buttonCode))
		{
			return true;
		}
		return false;
	}

	//コントローラーのボタンを今放したか調べる
	bool IsPadButtonUp(int buttonCode, int padID)
	{
		//今押してなくて、前回は押してる
		if (!IsPadButton(buttonCode, padID) && prevControllerState_[padID].Gamepad.wButtons & buttonCode)
		{
			return true;
		}
		return false;
	}

	float GetAnalogValue(int raw, int max, int deadZone)
	{
		float result = (float)raw;

		if (result > 0)
		{
			//デッドゾーン
			if (result < deadZone)
				result = 0;
			else
				result = (result - deadZone) / (max - deadZone);
		}

		else
		{
			//デッドゾーン
			if (result > -deadZone)
				result = 0;
			else
				result = (result + deadZone) / (max - deadZone);
		}

		return result;
	}


	//左スティックの傾きを取得
	XMVECTOR GetPadStickL(int padID)
	{
		//sThumbLX: 左スティックの横方向の傾き具合
		float x = GetAnalogValue(controllerState_[padID].Gamepad.sThumbLX, 32767, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		//sThumbLY: 左スティックの縦方向の傾き具合
		float y = GetAnalogValue(controllerState_[padID].Gamepad.sThumbLY, 32767, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);

		//傾き具合
		return XMVectorSet(x, y, 0, 0);
	}

	//右スティックの傾きを取得
	XMVECTOR GetPadStickR(int padID)
	{
		//sThumbRX: 右スティックの横方向の傾き具合
		float x = GetAnalogValue(controllerState_[padID].Gamepad.sThumbRX, 32767, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
		//sThumbRY: 右スティックの縦方向の傾き具合
		float y = GetAnalogValue(controllerState_[padID].Gamepad.sThumbRY, 32767, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);

		//傾き具合
		return XMVectorSet(x, y, 0, 0);
	}

	//左トリガーの押し込み具合を取得
	float GetPadTrrigerL(int padID)
	{
		//bLeftTrigger: 左トリガーの押し込み具合
		return GetAnalogValue(controllerState_[padID].Gamepad.bLeftTrigger, 255, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
	}

	//右トリガーの押し込み具合を取得
	float GetPadTrrigerR(int padID)
	{
		//bRightTrigger: 右トリガーの押し込み具合
		return GetAnalogValue(controllerState_[padID].Gamepad.bRightTrigger, 255, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
	}


	//解放
	void Release()
	{
		SAFE_RELEASE(pMouseDevice_);
		SAFE_RELEASE(pKeyDevice);
		SAFE_RELEASE(pDInput_);
	}
}