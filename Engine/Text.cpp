#include "Text.h"
#include <typeinfo>

//初期化
void Text::Initialize()
{
	//デバイスコンテキスト
	pDeviceContext_ = Direct3D::GetD2DDeviceContext();

	//ブラシ
	pSolidBrush_ = Direct3D::GetSolidBrush();

	//テキストフォーマット
	pTextFormat_ = nullptr;

	//フォントサイズ
	fontSize = 20;

	//todo エラー処理
	Direct3D::CreateTextFormat(&pTextFormat_, fontSize);
}



//テキスト描画
template <typename T>
void Text::DrowText(float x, float y, D2D1::ColorF::Enum color, float size, const T text)
{
	//最大文字数(とりあえず256ビット)
	const int maxStr = 256;

	//文字列格納配列
	WCHAR wcText[maxStr] = { 0 };

	//型判別
	{
		//判別用文字列準備
		auto i = typeid(int).name()[0];			//整数
		auto f = typeid(float).name()[0];		//浮動小数
		auto s = typeid(std::string).name()[0]; //文字列

		//型名取得(頭文字だけ)
		auto typeName = typeid(text).name()[0];

		//型別分岐でテキス内容準備
		if (typeName == i)
		{		    
			swprintf(wcText, maxStr, L"%1d", text);
		}
		else if (typeName == f)
		{
			swprintf(wcText, maxStr, L"%1f", text);
		}
		else if (typeName == s)
		{
			swprintf(wcText, maxStr, L"%1c", text);
		}
	}

	//描画
	{
		//ブラシ設定
		pSolidBrush_->SetColor(D2D1::ColorF(color));
		
		//描画
		pDeviceContext_->DrawTextA(
			wcText,
			ARRAYSIZE(wcText) - 1,
			pTextFormat_,
			D2D1::RectF(x, y, Direct3D::scrWidth, Direct3D::scrHeight),
			pSolidBrush_,
			D2D1_DRAW_TEXT_OPTIONS_NONE);

		//pDeviceContext_->EndDraw();
	}

}


//テンプレートをヘッダと分けたのでここに T の予測される型を記述する
template void Text::DrowText<int>(float x, float y, D2D1::ColorF::Enum color, float size, int text);
template void Text::DrowText<float>(float x, float y, D2D1::ColorF::Enum color, float size, float text);
template void Text::DrowText<std::string>(float x, float y, D2D1::ColorF::Enum color, float size, std::string text);

