#pragma once
#include "GameObject.h"

enum class SCENE_ID : int
{
	PREPLAY = 0,
	PLAY,
};

class SceneManager : public GameObject
{
public:
	SceneManager();
	SceneManager(GameObject* parent);	//ゲームオブジェクトを継承するコンストラクタ
	~SceneManager();

	//現在のシーン
	SCENE_ID currentSceneID_;
	//次のシーン
	SCENE_ID nextSceneID_;
	//シーン切り替え(セッターの役割)
	void ChangeScene(SCENE_ID nextID);

	// GameObject を介して継承されました
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

