#include <Windows.h>

#include "Engine/SceneManager.h"
#include "PrePlayScene.h"

//コンストラクタ
PrePlayScene::PrePlayScene(GameObject * parent)
	: GameObject(parent, "PrePlayScene")
{
}

//初期化
void PrePlayScene::Initialize()
{
}

//更新
void PrePlayScene::Update()
{
	//確認メッセージ表示
	int cheak = MessageBox(nullptr, "Start", "確認", MB_OK);
	if (cheak != IDOK)
		Sleep(0);

	//確認後プレイシーンに
	SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
	pSceneManager->ChangeScene(SCENE_ID::PLAY);

}

//描画
void PrePlayScene::Draw()
{
}

//開放
void PrePlayScene::Release()
{
}