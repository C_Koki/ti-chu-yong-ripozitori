//コンスタントバッファ
cbuffer global
{
    float4x4 WVP;
};

//頂点シェーダ
float4 VS(float4 pos : POSITION) :SV_Position
{
    return mul(pos, WVP);
}

//ピクセルシェーダ
float4 PS(float4 pos : SV_POSITION) :SV_Target
{
    //白色でワイヤーフレーム風に
    return float4(1, 1, 1, 0.3);
}