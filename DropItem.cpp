#include "DropItem.h"

DropItem::DropItem(GameObject* parent)
{
}

DropItem::~DropItem()
{
}

void DropItem::Initialize()
{
	//優先度設定
	SetPriority(ITEM_PRIORITY);
}

void DropItem::Update()
{
}

void DropItem::Draw()
{
	/* アイテムモデル描画 */
}

void DropItem::Release()
{
}

void DropItem::OnCollision(GameObject* pTarget)
{
	/* 触れたオブジェクト(キャラクタ)の移動速度上昇 */

	//現在の移動速度取得
	float TragetMoveSpeed = pTarget->GetMoveSpeed();

	//速度を上げてパラメータセット
	pTarget->SetMoveSpeed(TragetMoveSpeed * 1.1f);
}
