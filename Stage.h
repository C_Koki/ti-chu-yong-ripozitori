#pragma once
#include "Engine/GameObject.h"
#include <vector>

class CsvReader;


//ステージを管理するクラス
class Stage : public GameObject
{
	//モデルタイプの列挙
	enum { FLOOR, WALL, MAX };
private:

	//モデル番号
	int hModel_[MAX];

	//動的にtable_二次配列を確保
	int** table_;

	//壁モデルハンドルを格納するリスト
	std::vector<int> hWallModelList_;


public:
	//マップの大きさ
	int stageWidth_;
	int stageHeight_;

	//コンストラクタ
	Stage(GameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//壁床判断
	//引数：�@壁のx座標
	//		�A壁のy座標
	//戻値：壁の有無
	bool IsWall(int x, int z);

	//モデルハンドルの取得(壁)
	//戻値：モデルハンドル(壁)
	int GetModelWallHandle() { return hModel_[WALL]; }

	//壁モデルハンドルをリストに格納
	//引数：モデルハンドル(番号)
	void StoreWallHandle(int handle) { hWallModelList_.push_back(handle); }

	//壁モデルハンドルリストの受け渡し
	//戻値：モデルハンドルのリスト
	std::vector<int>* GetWallHandleList() { return &hWallModelList_; }

	//ステージ全体の壁モデルのハンドルを位置別に格納
	void StoreAllWallHandle(int hundle);


};