Texture2D		g_texture: register(t0);	//テクスチャー
SamplerState	g_sampler : register(s0);	//サンプラー

// コンスタントバッファ
// DirectX 側から送信されてくる、ポリゴン頂点以外の諸情報の定義
cbuffer global
{
	float4x4	matWVP;				// ワールド・ビュー・プロジェクションの合成行列
	float4x4	matNormalTrans;		// 法線の変換行列（回転行列と拡大の逆行列）
	float4x4	matWorld;			// ワールド変換行列
	float4		vecLightDir;		// ライトの方向ベクトル
	float4		vecDiffuse;			// ディフューズカラー（マテリアルの色）
	float4		vecAmbient;			// アンビエントカラー（影の色）
	float4		vecSpeculer;		// スペキュラーカラー（ハイライトの色）
	float4		vecCameraPosition;	// 視点（カメラの位置）
	float		shuniness;			// ハイライトの強さ（テカリ具合）
	bool		isTexture;			// テクスチャ貼ってあるかどうか

};

// 頂点シェーダー出力＆ピクセルシェーダー入力データ構造体
struct VS_OUT
{
	float4 pos    : SV_POSITION;	//位置
	float4 normal : TEXCOORD2;		//法線
	float2 uv	  : TEXCOORD0;		//UV座標
	float4 eye	  : TEXCOORD1;		//視線
};

// 頂点シェーダ
VS_OUT VS(float4 pos : POSITION, float4 Normal : NORMAL, float2 Uv : TEXCOORD)
{
	//ピクセルシェーダーへ渡す情報
	VS_OUT outData;

	//ローカル座標に、ワールド・ビュー・プロジェクション行列をかけて
	//スクリーン座標に変換し、ピクセルシェーダーへ
	outData.pos = mul(pos, matWVP);

	//法線の変形
	Normal.w = 0;							//4次元目は使わないので0
	Normal = mul(Normal, matNormalTrans);	//オブジェクトが変形すれば法線も変形
	outData.normal = Normal;				//これをピクセルシェーダーへ

	//視線ベクトル（ハイライトの計算に必要
	float4 worldPos = mul(pos, matWorld);					//ローカル座標にワールド行列をかけてワールド座標へ
	outData.eye = normalize(vecCameraPosition - worldPos);	//視点から頂点位置を引き算し視線を求めてピクセルシェーダーへ

	//UV「座標
	outData.uv = Uv;	//そのままピクセルシェーダーへ


	//まとめて出力
	return outData;
}

// ピクセルシェーダ
float4 PS(VS_OUT inData) : SV_Target
{
	//ライトの向き
	float4 lightDir = vecLightDir;	//グルーバル変数は変更できないので、いったんローカル変数へ
	lightDir = normalize(lightDir);	//向きだけが必要なので正規化

	//法線はピクセルシェーダーに持ってきた時点で補完され長さが変わっている
	//正規化しておかないと面の明るさがおかしくなる
	inData.normal = normalize(inData.normal);

	//拡散反射光（ディフューズ）
	//法線と光のベクトルの内積が、そこの明るさになる
	float4 shade = saturate(dot(inData.normal, -lightDir));
	shade.a = 1;	//暗いところが透明になるので、強制的にアルファは1

	float4 diffuse;
	//テクスチャ有無
	if (isTexture == true)
	{
		//テクスチャの色
		diffuse = g_texture.Sample(g_sampler, inData.uv);
	}
	else
	{
		//マテリアルの色
		diffuse = vecDiffuse;
	}

	//環境光（アンビエント）
	//これはMaya側で指定し、グローバル変数で受け取ったものをそのまま
	float4 ambient = vecAmbient;

	//鏡面反射光（スペキュラー）
	float4 speculer = float4(0, 0, 0, 0);	//とりあえずハイライトは無し
	if (vecSpeculer.a != 0)	//スペキュラーの情報があれば
	{
		float4 R = reflect(lightDir, inData.normal);			//正反射ベクトル
		speculer = pow(saturate(dot(R, inData.eye)), shuniness) * vecSpeculer;	//ハイライトを求める
	}

	//最終的な色
	return diffuse * shade + diffuse * ambient + speculer;
}
