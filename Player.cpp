#include <iostream>

#include "Player.h"
#include "Stage.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/Camera.h"
#include "Engine/SphereCollider.h"


//当たり判定の半径
const float PLAYER_RADIAUS = 0.4f;
//壁の大きさ
const float WALL_SIZE = 0.3f;
//移動量
const float MOVE = 0.1f;
//初期位置
CXMVECTOR PLAYER_POSITION = XMVectorSet(10.f, 0.f, 4.f, 0.f);

//コンストラクタ
Player::Player(GameObject * parent)
	:GameObject(parent, "Player"), hModel_(-1),pStage_(nullptr)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//初期位置設定
	transform_.position_ = PLAYER_POSITION;

	//モデル読み込み
	hModel_ = Model::Load("Assets/Player.fbx", SHADER_TOON);
	assert(hModel_ >= 0);
	
	//コライダー追加
	SphereCollider *collider = new SphereCollider(g_XMZero, PLAYER_RADIAUS);
	AddCollider(collider);

	//優先度設定
	SetPriority(PLAYER_PRIORITY);
}

//更新
void Player::Update()
{

	//移動処理
	Move();

}

//描画
void Player::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
}

//移動
void Player::Move()
{

	//行先ベクトル({0,0,0,0})
	XMVECTOR move = g_XMZero;
	
	//正面ベクトル({0,0,1,0})
	XMVECTOR front = g_XMIdentityR2;

	//入力に応じて移動量加算
	if (Input::IsKey(DIK_W))
	{
		move.vecZ += MOVE;
	}
	if (Input::IsKey(DIK_S))
	{
		move.vecZ -= MOVE;
	}
	if (Input::IsKey(DIK_A))
	{
		move.vecX -= MOVE;
	}
	if (Input::IsKey(DIK_D))
	{
		move.vecX += MOVE;
	}

	//移動処理
	transform_.position_ += move;

	//めり込み防止
	SaveDig(move);

}


//壁めり込み防止
void Player::SaveDig(CXMVECTOR move)
{
	//Stageを使うため
	pStage_ = (Stage*)FindObject("Stage");
	assert(pStage_ != nullptr);

	//移動先
	XMVECTOR cheakPos = transform_.position_ + move;

	//座標チェック用の変数
	int cheakX, cheakZ;

	//上(奥)の判定
	cheakX = cheakPos.vecX;
	cheakZ = cheakPos.vecZ + WALL_SIZE;
	if (pStage_->IsWall(cheakX, cheakZ))
	{
		transform_.position_.vecZ = cheakZ - WALL_SIZE;
	}

	//下(手前)の判定
	cheakX = cheakPos.vecX;
	cheakZ = cheakPos.vecZ - WALL_SIZE;
	if (pStage_->IsWall(cheakX, cheakZ))
	{
		transform_.position_.vecZ = cheakZ + 1 + WALL_SIZE;
	}

	//左の判定
	cheakX = cheakPos.vecX - WALL_SIZE;
	cheakZ = cheakPos.vecZ;
	if (pStage_->IsWall(cheakX, cheakZ))
	{
		transform_.position_.vecX = cheakX + 1 + WALL_SIZE;
	}

	//右の判定
	cheakX = cheakPos.vecX + WALL_SIZE;
	cheakZ = cheakPos.vecZ;
	if (pStage_->IsWall(cheakX, cheakZ))
	{
		transform_.position_.vecX = cheakX - WALL_SIZE;
	}
}
