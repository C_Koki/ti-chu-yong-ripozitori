#include <iostream>

#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
//#include "Engine/CharacterAI.h"
#include "Engine/SceneManager.h"
#include "EnemyCrawl.h"
#include "Stage.h"

//当たり判定の半径
const float Enemy_RADIAUS = 0.4f;
//引力の大きさ
const int ATTRACT = 10;
//斥力の大きさ
const int REPULS = 2;
//引力斥力の範囲は今は考えない
const int A_DECAY = 0;
const int R_DECAY = 0;
//初期位置
CXMVECTOR EnemyCrawl_POSITION = XMVectorSet(4.f, 0.f, 7.f, 0.f);
//移動速度
const float ENEMY_SPEED = 0.1f;


//コンストラクタ
EnemyCrawl::EnemyCrawl(GameObject* parent)
	:GameObject(parent, "EnemyCrawl"), hModel_(-1)
	, pAI_(new CharacterAI())
{
}

//デストラクタ
EnemyCrawl::~EnemyCrawl()
{
}

//初期化
void EnemyCrawl::Initialize()
{
	//モデル読み込み
	hModel_ = Model::Load("Assets/Enemy_blue.fbx", SHADER_TOON);
	assert(hModel_ >= 0);

	//配置
	transform_.position_ = EnemyCrawl_POSITION;
	transform_.Calclation();

	//コライダーの定義
	SphereCollider* collider = new SphereCollider(g_XMZero, Enemy_RADIAUS);

	//コライダーセット
	AddCollider(collider);

	//AIのセット
	AddCharacterAI(pAI_, ENEMY_SPEED);

	//ステートの設定
	pAI_->SetState(AI_STATE::STOP_AND_RESTART);
}

//更新
void EnemyCrawl::Update()
{
	//対戦相手(プレイヤー)をセット
	pAI_->SetOpponentObject(pParent_->FindObject("Player"));

	//レイ発射
	pAI_->SetRayToAvoidTarget((Stage*)FindObject("Stage"));

	//AIアクティブ化
	pAI_->Active();
}

//描画
void EnemyCrawl::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);

#ifdef _DEBUG
	pAI_->Draw();
#endif // _DEBUG

}

//開放
void EnemyCrawl::Release()
{
}

//当たり判定(コリジョン)
void EnemyCrawl::OnCollision(GameObject* pTarget)
{
	//プレイヤーに当たったら
	if (pTarget->GetObjectName() == "Player")
	{
		//デバッグ用
#ifdef _DEBUG
		OutputDebugString("OK\n");
#endif // _DEBUG

		//当たったらプレイシーンリセット
		int cheak = MessageBox(nullptr, "Catched!", "確認", MB_OK);

		if (cheak != IDOK)
			Sleep(0);

		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID::PREPLAY);

	}
}
