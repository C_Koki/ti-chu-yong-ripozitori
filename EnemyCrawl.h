#pragma once
#include "Engine/GameObject.h"
#include "Engine/Math.h"
#include "Engine/CharacterAI.h"


//テスト用
class EnemyCrawl : public GameObject
{
	//モデル番号
	int hModel_;

	//キャラクターAI
	CharacterAI* pAI_;

	//正面ベクトル(EnemyCrawl自身のローカル座標)
	XMVECTOR front_ = g_XMIdentityR2;

public:
	//コンストラクタ
	EnemyCrawl(GameObject* parent);

	//デストラクタ
	~EnemyCrawl();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//当たった時
	//引数：当たったオブジェクト
	void OnCollision(GameObject* pTarget) override;
};