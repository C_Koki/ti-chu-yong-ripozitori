#pragma once
#include "Engine/GameObject.h"

class Stage;

//テスト用
class Player : public GameObject
{
	//モデルハンドル
	int hModel_;

	//ステージ用変数
	Stage* pStage_;

	//優先度
	const int PLAYER_PRIORITY = 1;

public:
	//コンストラクタ
	Player(GameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//移動
	void Move();

	//めり込み防止
	void SaveDig(CXMVECTOR move);

	//モデルハンドルの取得(プレイヤー)
	//引数：なし
	//戻値：モデルハンドル(プレイヤー)
	int GetModelPlayerHandle() { return hModel_; }

	
};