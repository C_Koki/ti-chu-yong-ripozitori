#pragma once
#include "Engine/GameObject.h"
#include "Engine/Math.h"
#include "Engine/CharacterAI.h"


//テスト用
class Enemy : public GameObject
{
	//モデル番号
	int hModel_;

	//キャラクターAI
	CharacterAI* pAI_;

	//正面ベクトル(Enemy自身のローカル座標)
	XMVECTOR front_ = g_XMIdentityR2;

	//移動速度
	const float ENEMY_SPEED = 0.08f;

public:
	//コンストラクタ
	Enemy(GameObject* parent);

	//デストラクタ
	~Enemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//当たった時
	//引数：当たったオブジェクト
	void OnCollision(GameObject* pTarget) override;

	//移動速度を渡す
	//戻値：移動速度
	float GetEnemySpeed() { return ENEMY_SPEED; }
};