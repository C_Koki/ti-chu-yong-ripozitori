#include <Windows.h>
#include <stdlib.h>

#include "Engine/Direct3D.h"
#include "Engine/Input.h"
//#include "Engine/Transform.h"
#include "Engine/RootJob.h"
#include "Engine/Global.h"

#pragma comment(lib,"winmm.lib")

//定数宣言
const char* WIN_CLASS_NAME = "SampleGame";	//ウィンドウクラス名
const int WINDOW_WIDTH = 800;				//ウィンドウの幅
const int WINDOW_HEIGHT = 600;				//ウィンドウの高さ
const int FPS = 60;


//プロトタイプ宣言
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//ポインタ
RootJob* pRootjob = new RootJob;

//デバッグ時にコンソールが出せるように
#ifdef _DEBUG
int main()
{

#else
//エントリーポイント
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
#endif

	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);             //この構造体のサイズ
	wc.hInstance = GetModuleHandle(nullptr);    //インスタンスハンドル
	wc.lpszClassName = WIN_CLASS_NAME;          //ウィンドウクラス名
	wc.lpfnWndProc = WndProc;                   //ウィンドウプロシージャ
	wc.style = CS_VREDRAW | CS_HREDRAW;         //スタイル（デフォルト）
	wc.hIcon = LoadIcon(NULL, IDI_QUESTION);	//アイコン
	wc.hIconSm = LoadIcon(NULL, IDI_QUESTION);  //小さいアイコン
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);   //マウスカーソル
	wc.lpszMenuName = NULL;                     //メニュー（なし）
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH); //背景（白）
	RegisterClassEx(&wc);  //クラスを登録

	//ウィンドウサイズの計算
	RECT winRect = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, FALSE);
	int winW = winRect.right - winRect.left;     //ウィンドウ幅
	int winH = winRect.bottom - winRect.top;     //ウィンドウ高さ

	//ウィンドウを作成
	HWND hWnd = CreateWindow(
		WIN_CLASS_NAME,         //ウィンドウクラス名
		WIN_CLASS_NAME,		    //タイトルバーに表示する内容
		WS_OVERLAPPEDWINDOW,	//スタイル（普通のウィンドウ）
		CW_USEDEFAULT,			//表示位置左（おまかせ）
		CW_USEDEFAULT,		    //表示位置上（おまかせ）
		winW,				    //ウィンドウ幅
		winH,					//ウィンドウ高さ
		NULL,					//親ウインドウ（なし）
		NULL,					//メニュー（なし）
		wc.hInstance,			//インスタンス
		NULL					//パラメータ（なし）
	);

	//ウィンドウを表示
	ShowWindow(hWnd, SW_SHOW);

	//Direct3D初期化
	if (FAILED(Direct3D::Initialize(WINDOW_WIDTH, WINDOW_HEIGHT, hWnd)))
	{
		return 0;
	}	

	//DirectInputの初期化
	Input::Initialize(hWnd);

	//RootJobの初期化
	pRootjob->Initialize();

	//メッセージループ（何か起きるのを待つ）
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	while (msg.message != WM_QUIT)
	{
		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし
		else
		{
			//時間を正確に扱うため
			timeBeginPeriod(1);

			//FPS
			static DWORD countFps = 0;

			//static追加…最初の一回しか作動しない
			//開始時刻を取得
			static DWORD startTime = timeGetTime();

			//現在時刻を取得
			DWORD nowTime = timeGetTime();

			//最後に更新した時間を取得
			static DWORD lastUpdateTime = nowTime;

			if (nowTime - startTime >= 1000)
			{
				char str[16];
				wsprintf(str, "%u", countFps);

				//タイトルバーにテキスト表示(今回はFPS)
				SetWindowText(hWnd, str);

				//更新
				countFps = 0;
				startTime = nowTime;
			}

			//1000/60ミリ秒以下であれば時間経過とみなさない
			//これは１フレームの計測時間なので例えばこれに移動速度をかければ
			//一定の安定した速度になる
			if ((nowTime - lastUpdateTime) * FPS <= 1000)
			{
				continue;
			}

			//最終更新時刻更新
			lastUpdateTime = nowTime;

			//FPS計測
			countFps++;

#ifdef _DEBUG
			//Spaceでゆっくりにする
			if(Input::IsKey(DIK_SPACE))
			{
				Sleep(60);
			}
#endif // _DEBUG


			//ゲームの処理
			//キーが入力されているかの更新
			Input::Update();



			//更新処理(Input後が望ましい)
			pRootjob->UpdateSub();

			//Direct3Dの描画
			Direct3D::BeginDraw();

			//描画処理
			pRootjob->DrawSub();


			if (FAILED(Direct3D::EndDraw()))
			{
				return 0;
			}

			//Escで強制終了
			if (Input::IsKeyDown(DIK_ESCAPE))
			{
				break;
			}


			//少し休ませる
			Sleep(1);
			timeEndPeriod(1);
		}
	}


	//解放処理
	pRootjob->ReleaseSub();
	SAFE_DELETE(pRootjob);
	Input::Release();
	Direct3D::Release();

	return 0;
}

//ウィンドウプロシージャ（何かあった時によばれる関数）
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	//マウスのメッセージ処理
	case WM_MOUSEMOVE:
		Input::SetMousePosition(LOWORD(lParam), HIWORD(lParam));
		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);  //プログラム終了
		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}
