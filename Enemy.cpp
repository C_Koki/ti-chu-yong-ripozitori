#include <iostream>

//#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
//#include "Engine/CharacterAI.h"
#include "Engine/SceneManager.h"
#include "Enemy.h"
#include "Stage.h"

//当たり判定の半径
const float ENEMY_RADIAUS = 0.4f;
//引力の大きさ
const int ATTRACT = 10;
//斥力の大きさ
const int REPULS = 2;
//引力斥力の範囲は今は考えない
const int A_DECAY = 0;
const int R_DECAY = 0;
//初期位置
CXMVECTOR ENEMY_POSITION = XMVectorSet(6.f, 0.f, 12.f, 0.f);

//コンストラクタ
Enemy::Enemy(GameObject * parent)
	:GameObject(parent, "Enemy"), hModel_(-1)
	,pAI_(new CharacterAI())
{
	//移動速度設定(でもこれだとEnemyObject基底とこいつ自身に二つSpeed格の変数が存在する)
	SetMoveSpeed(ENEMY_SPEED);
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデル読み込み
	hModel_ = Model::Load("Assets/Enemy.fbx", SHADER_TOON);
	assert(hModel_ >= 0);

	//配置
	transform_.position_ = ENEMY_POSITION;
	transform_.Calclation();

	//コライダーの定義
	SphereCollider *collider = new SphereCollider(g_XMZero, ENEMY_RADIAUS);

	//コライダーセット
	AddCollider(collider);

	//AIのセット
	//ここで自身の移動速度、追跡パターンを設定
	AddCharacterAI(pAI_, ENEMY_SPEED);

	
}

//更新
void Enemy::Update()
{
	//対戦相手(プレイヤー)をセット
	pAI_->SetOpponentObject(pParent_->FindObject("Player"));

	//レイ発射(対象は避けるオブジェクト)
	pAI_->SetRayToAvoidTarget((Stage*)FindObject("Stage"));

	//ステートの設定(後で使いやすいように纏めたい)
	pAI_->SetState(AI_STATE::TRACK_AND_AVOID);

	//AIのアクティブ化
	pAI_->Active();
}

//描画
void Enemy::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);

#ifdef _DEBUG
	//正面方向のレイ描画
	pAI_->Draw();
#endif // _DEBUG

}

//開放
void Enemy::Release()
{
}

//当たり判定(コリジョン)
void Enemy::OnCollision(GameObject * pTarget)
{
	//プレイヤーに当たったら
	if (pTarget->GetObjectName() == "Player")
	{
		//デバッグ用
#ifdef _DEBUG
		OutputDebugString("OK\n");
#endif // _DEBUG

		//当たったらプレイシーンリセット
		int cheak = MessageBox(nullptr, "Catched!", "確認", MB_OK);

		if (cheak != IDOK)
			Sleep(0);

		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID::PREPLAY);

	}
}
